#include "at24c02.h"

extern BYTE I2CBUS;

void EEPROM_WR_ON(void)
{
    EEPROM_WP=1;    //打开写保护，只能读
}

void EEPROM_WR_OFF(void)
{
    EEPROM_WP=0;    //关闭写保护，可读可写
}

void EEPROM_WriteByte(unsigned char addr, unsigned char dat)
{
	I2CBUS = EEP_SI_I2CBUS ;
	EEPROM_WR_OFF();
	hlWriteByte_8BA (EEPROM_ADDR, addr, dat);
	EEPROM_WR_ON();
	I2CBUS = SiI_I2CBUS ;
	delay_ms(5);	//如果EEPROM不能正常读取，请更改延时时间（EEPROM写完之后需延时一段时间再读取）
}

unsigned char EEPROM_ReadByte(unsigned char addr)
{
    unsigned char GetData=0;

	I2CBUS = EEP_SI_I2CBUS ;
	GetData=hlReadByte_8BA (EEPROM_ADDR, addr);
	I2CBUS = SiI_I2CBUS ;

	return GetData;
}

