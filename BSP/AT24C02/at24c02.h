#ifndef __AT24C02_H
#define __AT24C02_H	 
#include <stc11lxx.h>
#include "i2c.h"
#include "includes_bsp.h"

#define EEPROM_WP    P3_6	 //写保护 接高电平实现写保护（只读）；接低电平或悬空可进行读写操作

#define EEP_SI_SCL	 P1_0	 //EEPROM和SiI9287共用一个I2C
#define EEP_SI_SDA	 P1_1

#define EEPROM_ADDR  0xA2	 //AT24C02地址


void EEPROM_WR_ON(void);
void EEPROM_WR_OFF(void);
void EEPROM_WriteByte(unsigned char addr, unsigned char dat);
unsigned char EEPROM_ReadByte(unsigned char addr);
	 				    
#endif
