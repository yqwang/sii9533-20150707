#include "i2c.h"

static void I2C_delay(void)
{	
    BYTE i=500;                  //这里可以优化速度，经测试最低到5还能写入
   
    while(i) 
    { 
        i--; 
    } 
}

BYTE I2CBUS = SiI_I2CBUS ;	//选择使用哪个I2C通道

#if 0

static void halSetSDAPin(void)
{		
	SiI_SDA = 1;
}

static void halSetSCLPin(void)
{		
	SiI_SCL = 1;
}

static void halClearSDAPin(void)
{		
	SiI_SDA = 0;
}

static void halClearSCLPin(void)
{		
	SiI_SCL = 0;
}

BOOL halGetSDAPin(void)
{		
	return SiI_SDA;
}

BOOL halGetSCLPin(void)
{
	return SiI_SCL;
}

#endif



#if 1
static void halSetSDAPin(void)
{
	if(I2CBUS == SiI_I2CBUS)
	{		
		SiI_SDA = 1;
	}
	else if(I2CBUS == DDC_I2CBUS)
	{
		DDC_SDA= 1;
	}
	else if(I2CBUS == EEP_SI_I2CBUS)
	{
	    EEP_SI_SDA = 1;
	}

}

static void halSetSCLPin(void)
{

	if(I2CBUS == SiI_I2CBUS)
	{		
		SiI_SCL = 1;
	}
	else if(I2CBUS == DDC_I2CBUS)
	{
		DDC_SCL= 1;
	}
    else if(I2CBUS == EEP_SI_I2CBUS)
	{
	    EEP_SI_SCL = 1;
	}


}

static void halClearSDAPin(void)
{

	if(I2CBUS == SiI_I2CBUS)
	{		
		SiI_SDA = 0;
	}
	else if(I2CBUS == DDC_I2CBUS)
	{
		DDC_SDA= 0;
	}
	else if(I2CBUS == EEP_SI_I2CBUS)
	{
	    EEP_SI_SDA = 0;
	}

}

static void halClearSCLPin(void)
{
	if(I2CBUS == SiI_I2CBUS)
	{		
		SiI_SCL = 0;
	}
	else if(I2CBUS == DDC_I2CBUS)
	{
		DDC_SCL = 0;
	}
	else if(I2CBUS == EEP_SI_I2CBUS)
	{
	    EEP_SI_SCL = 0;
	}

}

BOOL halGetSDAPin(void)
{

 	if(I2CBUS == SiI_I2CBUS)
	{		
		return SiI_SDA;
	}
	else if(I2CBUS == DDC_I2CBUS)
	{
		return DDC_SDA;
	}
    else if(I2CBUS == EEP_SI_I2CBUS)
	{
	    return EEP_SI_SDA;
	}
	else
		return 0;


}

BOOL halGetSCLPin(void)
{
	if(I2CBUS == SiI_I2CBUS)
	{
		
		return SiI_SCL;
	}
	else if(I2CBUS == DDC_I2CBUS)
	{
		return DDC_SCL;
	}
	else if(I2CBUS == EEP_SI_I2CBUS)
	{
	    return EEP_SI_SCL;
	}
	else
		return 0;
}

#endif

/***************************************************************************************************/

BYTE GetI2CState(void)
{
    BYTE i,j;

	halSetSCLPin();
    for( i = 0; i < 10; i++ )
    {
        if( halGetSCLPin() )
        {
		   halSetSDAPin();
            for( j = 0; j < 10; j++ )
            {
                if( halGetSDAPin() )
                return 0;   // OK
            }
            return 1;       // SCL OK, SDA someone pull down
        }
    }
    return 2;               // error, someone holds the SCL bus
}

//-----------------------------------------------------------------------------------
void _I2CSCLHigh(void)    // set SCL high, and wait for it to go high
{       
    BYTE err;

    halSetSCLPin();
    while ( !halGetSCLPin() )
    {
        err++;
        if ( !err ) 
        {
            //_i2c_error &= 0x02;     // SCL stuck, something's holding it down
            return;
        }
    }
}

// First I2C Port
BYTE I2CSendByte(BYTE bt)
{
    unsigned char i;
    unsigned char error=0;     // if error =1 then there was an error in getting ACK

    for ( i = 0; i < 8; i++) 
    {
        if (bt & 0x80)
			halSetSDAPin();
        else
			halClearSDAPin();
	    I2C_delay();
        _I2CSCLHigh();
        I2C_delay();
		halClearSCLPin();
        bt = bt << 1;
        I2C_delay(); 
    }                  
	halSetSDAPin();	 // listen for ACK
    _I2CSCLHigh();
    I2C_delay();
    I2C_delay();
    I2C_delay();
      
    if ( halGetSDAPin() )    //是否有应答信号
        error=1;
	halClearSCLPin();
    I2C_delay();
   
    return error;     // return 0 if no error, else return 1
}
//------------------------------------------------------------------------------
BYTE I2CSendAddr( BYTE addr, BYTE rd )
{
    unsigned char error=0;

    halSetSCLPin();
    I2C_delay();
    //halSetSCLPin(cInstance);           // Restart was too short
    //I2C_delay();
    I2C_delay();
    halClearSDAPin();                            // generate start
    //halClearSDAPin(cInstance);        // Restart was too short
    I2C_delay(); 
    I2C_delay(); 
    halClearSCLPin();
    I2C_delay(); 
    error = I2CSendByte( addr+rd );     // send address byte
    
    return error;
}
//----------------------------------------------------------------------------
BYTE _I2CGetByte(BYTE lastone) 
{ // lastone == 1 for last byte
    BYTE i, res;

    res = 0;
    for ( i = 0; i < 8; i++ ) 
    {          // each bit at a time, MSB first
        _I2CSCLHigh();
        I2C_delay();
        res *= 2;
        if ( halGetSDAPin() )
            res++;
        halClearSCLPin();
        I2C_delay();
    }
    if ( lastone )//[kyq Begin: 20140625 11:23:09]
        halSetSDAPin();                  
    else
        halClearSDAPin(); // send ACK according to 'lastone'
    I2C_delay(); 
    _I2CSCLHigh();
    I2C_delay(); 
	halClearSCLPin();
    I2C_delay(); 
    halSetSDAPin();
    
    return  res;
}

void I2CSendStop(void)
{
	halClearSDAPin();
    I2C_delay(); 
    _I2CSCLHigh();
    I2C_delay(); 
	halSetSDAPin();
    I2C_delay(); 
}

/************************************I2C读取一个字节*******************************************/
BYTE hlReadByte_8BA ( BYTE SlaveAddr, BYTE RegAddr )
{
    BYTE Data;
    BYTE cError =0;

    cError = I2CSendAddr(SlaveAddr,WRITE);
    if( cError )
    {
        //UART_SendString("I2C Send SlaveAddr error \n");              
    }
    cError = I2CSendByte(RegAddr);
    if( cError )
    {
        //UART_SendString("I2C Send RegAddr error \n");               
    }
    cError = I2CSendAddr (SlaveAddr,READ);
    if( cError )
    {
        //UART_SendString("I2C Send ReadCom error \n");               
    }
    Data = I2CGetLastByte();
    I2CSendStop();
    
    return Data;
}

/****************************************I2C写一个字节********************************************/
BYTE hlWriteByte_8BA ( BYTE SlaveAddr, BYTE RegAddr, BYTE Data ) 
{
    BYTE bState;
    //if(SlaveAddr == TX_SLV1 && RegAddr == 0xDF)
    //printf(" @ 0x%bx= 0x%bx\r\n",RegAddr,Data);
    bState = GetI2CState();
    if( bState )
    {
        //BSP_OS_SemPost(&g_sgI2C[cInstance]);
        return IIC_CAPTURED;
    }
    bState = I2CSendAddr( SlaveAddr,WRITE);
    if( bState )
    {
        I2CSendStop();
        //BSP_OS_SemPost(&g_sgI2C[cInstance]);
        //APP_TRACE("I2C wrt: 0x%2x Send Addrr:0x%2x Error!\r\n",cInstance,SlaveAddr);
        return IIC_NOACK;
    }
    bState = I2CSendByte(RegAddr);
    if( bState )
    {
        I2CSendStop();
        //BSP_OS_SemPost(&g_sgI2C[cInstance]);
        //APP_TRACE("I2C wrt(0x%2x)Error: Addr:0x%2x,Reg:0x%2x\r\n",cInstance,SlaveAddr,RegAddr);
        return IIC_NOACK;
    }
    bState = I2CSendByte(Data);
    if( bState )
    {
        I2CSendStop();
        //BSP_OS_SemPost(&g_sgI2C[cInstance]);
        //APP_TRACE("I2C wrt(0x%2x)Error: Addr:0x%2x,Reg:0x%2x,value:0x%2x !\r\n",cInstance,SlaveAddr,RegAddr,Data);
        return IIC_NOACK;
    }	
    I2CSendStop();
    //BSP_OS_SemPost(&g_sgI2C[cInstance]);
    
    return 0;
}

/**************************************I2C读取一个字********************************************/
WORD hlReadWord_8BA( BYTE SlaveAddr, BYTE RegAddr ) 
{
    WORD Data;
    BYTE cError =0;

    cError = I2CSendAddr( SlaveAddr, WRITE );      
    cError |= I2CSendByte( RegAddr++);
    cError |= I2CSendAddr( SlaveAddr, READ );
    Data = I2CGetByte();				  //先读低8位
    Data |= (I2CGetLastByte() << 8 );     //再读高8位
    I2CSendStop();
    if( cError )
    {
        //UART_SendString("I2C ReadWord error \n");            
    }
    
    return Data;
}

/**************************************I2C写一个字**********************************************/
void hlWriteWord_8BA( BYTE SlaveAddr, BYTE RegAddr, WORD Data )
{
    BYTE bState = 0;
    
    bState = I2CSendAddr(SlaveAddr,WRITE);
    bState |= I2CSendByte(RegAddr);
    bState |=I2CSendByte(Data & 0xff);
    bState |=I2CSendByte(Data >> 8);
    I2CSendStop();
    if( bState)
    {   
        //UART_SendString("I2C WriteWord error \n"); 
    }	
}

unsigned char I2C_BufferWrite_Number(unsigned char SlaveAddr,unsigned char RegAddr,unsigned char *Data,int number)	
{ 
	int i = 0;
	BYTE bState = 0;

    //if(SlaveAddr == TX_SLV1 && RegAddr == 0xDF)
    //printf(" @ 0x%bx= 0x%bx\r\n",RegAddr,Data);
    bState = GetI2CState();
    if( bState )
    {
        //BSP_OS_SemPost(&g_sgI2C[cInstance]);
        return 0xff;
    }
    bState = I2CSendAddr( SlaveAddr,WRITE);
    if( bState )
    {
        I2CSendStop();
        //BSP_OS_SemPost(&g_sgI2C[cInstance]);
        //APP_TRACE("I2C wrt: 0x%2x Send Addrr:0x%2x Error!\r\n",cInstance,SlaveAddr);
        return 0xff;
    }
    bState = I2CSendByte(RegAddr);
    if( bState )
    {
        I2CSendStop();
        //BSP_OS_SemPost(&g_sgI2C[cInstance]);
        //APP_TRACE("I2C wrt(0x%2x)Error: Addr:0x%2x,Reg:0x%2x\r\n",cInstance,SlaveAddr,RegAddr);
        return 0xff;
    }


    bState = I2CSendByte(Data);
    if( bState )
    {
        I2CSendStop();
        //BSP_OS_SemPost(&g_sgI2C[cInstance]);
        //APP_TRACE("I2C wrt(0x%2x)Error: Addr:0x%2x,Reg:0x%2x,value:0x%2x !\r\n",cInstance,SlaveAddr,RegAddr,Data);
        return 0xff;
    }	
    
    //BSP_OS_SemPost(&g_sgI2C[cInstance]);
	
	for(i = 0 ; i< number; i ++)
	{
	    bState = I2CSendByte(Data[i]);
	    if( bState )
	    {
	        I2CSendStop();
	        //BSP_OS_SemPost(&g_sgI2C[cInstance]);
	        //APP_TRACE("I2C wrt(0x%2x)Error: Addr:0x%2x,Reg:0x%2x,value:0x%2x !\r\n",cInstance,SlaveAddr,RegAddr,Data);
	        return 0xff;
	    }
	}
 	I2CSendStop();

	return 1; 
}




/*************************************************************************************************************************************/

/**************************************I2C读一个块***********************************************/
/*
BYTE hlBlockRead_8BAS( I2CShortCommandType * IIC, BYTE * Data )
{
    BYTE i, bState;
	
    if(!(IIC->Flags & FLG_CONTD)) 
	{
        bState = GetI2CState();
        if(bState)
	    {
            //BSP_OS_SemPost(&g_sgI2C[cInstance]);
            return IIC_CAPTURED;
        }
        bState = I2CSendAddr(IIC->SlaveAddr,WRITE);
        if(bState)
		{
            I2CSendStop();
            //BSP_OS_SemPost(&g_sgI2C[cInstance]);
            return IIC_NOACK;
        }
        bState = I2CSendByte(IIC->RegAddrL);
        if(bState) 
	    {
            I2CSendStop();
            //BSP_OS_SemPost(&g_sgI2C[cInstance]);
            return IIC_NOACK;
        }
        bState = I2CSendAddr (IIC->SlaveAddr,READ);
        if(bState) 
	    {
            I2CSendStop();
            //BSP_OS_SemPost(&g_sgI2C[cInstance]);
            return IIC_NOACK;
        }
    }
    for (i = 0; i < IIC->NBytes - 1; i++)
    {
        Data[i] = I2CGetByte();
    }
    if(IIC->Flags & FLG_NOSTOP)
    {
        Data[i] = I2CGetByte();
    }
    else 
    {
        Data[i] = I2CGetLastByte();
        I2CSendStop();
    }

    return IIC_OK;
}
*/
/*******************************************I2C写一个块**************************************/
/*
BYTE hlBlockWrite_8BAS( I2CShortCommandType * IIC, const BYTE * pcData )
{
    WORD i;
    BYTE bState;
	//BSP_OS_SemWait(&g_sgI2C[cInstance], 0);

    if(!(IIC->Flags & FLG_CONTD)) 
	{
        bState = GetI2CState();
        if( bState )
        {
            UART_SendString("EDID GetI2CState error \n");
            return IIC_CAPTURED;
        }
        bState = I2CSendAddr (IIC->SlaveAddr,WRITE);
        if( bState )
		{
            I2CSendStop();
            UART_SendString("EDID Send SlaveAddr error \n");
            return IIC_NOACK;
        }
        bState = I2CSendByte(IIC->RegAddrL);
        if(bState)
		{
            I2CSendStop();
            UART_SendString("EDID Send RegAddr error \n");
            return IIC_NOACK;
        }
    }
    for (i=0; i<IIC->NBytes; i++)
        I2CSendByte(pcData[i]);
    if(!(IIC->Flags & FLG_NOSTOP))
        I2CSendStop();
    //BSP_OS_SemPost(&g_sgI2C[cInstance]);
    return IIC_OK; 
}
 */
 /*
//------------------------------------------------------------------------------
// Function Name: siiReadBlockHDMI
// Function Description: Reads block of data from HDMI RX (page 1)
//------------------------------------------------------------------------------
// return:
//       IIC_OK
//       IIC_NOACK
//       IIC_CAPTURED
BYTE hlI2CDeviceReadBlock( BYTE bSlaveAddr, BYTE bAddr, WORD bNBytes, BYTE * pbData )
{
    I2CShortCommandType I2CComm;

    I2CComm.SlaveAddr = bSlaveAddr;
    I2CComm.Flags = 0;
    I2CComm.NBytes = bNBytes;
    I2CComm.RegAddrL = bAddr;

    return hlBlockRead_8BAS((I2CShortCommandType *)&I2CComm, pbData);
}

//------------------------------------------------------------------------------
// Function Name:  siiWriteBlockHDMI
// Function Description: Writes block of data from HDMI Device
//------------------------------------------------------------------------------
// return:
//       IIC_OK
//       IIC_NOACK
//       IIC_CAPTURED
BYTE hlI2CDeviceWriteBlock( BYTE bSlaveAddr, BYTE bRegAddr, WORD bNBytes, const BYTE *  pbData )
{
    I2CShortCommandType I2CComm;

    I2CComm.SlaveAddr = bSlaveAddr;
    I2CComm.Flags = 0;
    I2CComm.NBytes = bNBytes;
    I2CComm.RegAddrL = bRegAddr;
    I2CComm.RegAddrH= 0;

    return hlBlockWrite_8BAS((I2CShortCommandType *)&I2CComm, pbData);
}
*/
/*********************************************************************************************************************/





