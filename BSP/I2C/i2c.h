#ifndef __I2C_H
#define __I2C_H	 
#include <stc11lxx.h>
#include "includes_bsp.h"
#include "at24c02.h"
//#include "edid.h"

//
#define DDC_SDA	   P2_1
#define DDC_SCL    P2_2

#define SiI_SDA    P2_6
#define SiI_SCL    P2_7

//

#define EEP_SI_I2CBUS    0x02
#define DDC_I2CBUS       0x01
#define SiI_I2CBUS       0x00

#define WRITE            0x00
#define READ             0x01

#define FLG_SHORT        0x01 // Used for Ri Short Read
#define FLG_NOSTOP       0x02 // Don't release IIC Bus
#define FLG_CONTD        0x04 // Continued from previous operation

#define IIC_CAPTURED     1
#define IIC_NOACK        2
#define IIC_OK           0

#define I2CGetByte()     _I2CGetByte(0)
#define I2CGetLastByte() _I2CGetByte(1)

/*
typedef struct 
{
    BYTE SlaveAddr;		 //���豸��ַ
    BYTE Flags;			 //
    //BYTE NBytes;
    
    BYTE RegAddrL;		 //�Ĵ�����ַ���ֽ�
    BYTE RegAddrH;		 //�Ĵ�����ַ���ֽ�
    WORD NBytes;		 //��д�����ֽ�

} I2CShortCommandType;

typedef enum
{
    rcSUCCESS,
    rcERROR,
    rcINVALID
} eRESULT;
*/

void I2C_delay(void);
void I2C_Select(BYTE I2CBus);
BYTE GetI2CState( void );
void _I2CSCLHigh( void );
BYTE I2CSendByte( BYTE bt );
BYTE I2CSendAddr( BYTE addr, BYTE rd );
BYTE _I2CGetByte(BYTE lastone);
void I2CSendStop();
BYTE hlReadByte_8BA ( BYTE SlaveAddr, BYTE RegAddr );
WORD hlReadWord_8BA( BYTE SlaveAddr, BYTE RegAddr );
BYTE hlWriteByte_8BA ( BYTE SlaveAddr, BYTE RegAddr, BYTE Data );
void hlWriteWord_8BA( BYTE SlaveAddr, BYTE RegAddr, WORD Data );
//BYTE hlBlockRead_8BAS( I2CShortCommandType * IIC, BYTE * Data );
//BYTE hlBlockWrite_8BAS( I2CShortCommandType * IIC, const BYTE * pcData );
//BYTE hlI2CDeviceReadBlock( BYTE bSlaveAddr, BYTE bAddr, WORD bNBytes, BYTE * pbData );
//BYTE hlI2CDeviceWriteBlock( BYTE bSlaveAddr, BYTE bRegAddr, WORD bNBytes, const BYTE *  pbData );

unsigned char I2C_BufferWrite_Number(unsigned char SlaveAddr,unsigned char RegAddr,unsigned char *Data,int number)	;

		 				    
#endif

