#ifndef __INCLUDES_BSP_H
#define __INCLUDES_BSP_H


//--------------------------MCU--------------------------------//	 
#include <stc11lxx.h>
#include "type.h"
#include "mcu.h"
#include "stc_eeprom.h"


//---------------------------C---------------------------------//
#include <intrins.h>
#include <string.h>
#include <stdio.h>


//--------------------------BSP--------------------------------//
#include "bsp_led.h"
#include "i2c.h"
#include "at24c02.h"
#include "SiI_9533.h"
#include "SiI_9533_reg.h"



		 				    
#endif
