#include "SiI_9533.h"
#include "SiI_9533_reg.h"
#include "includes_bsp.h"
#include "i2c.h"

#if 1
typedef enum
{
    NVRAM_HDMI_EDID,
    NVRAM_VGA_EDID,
    NVRAM_BOOTDATA,
} SiiNvramType_t;

typedef enum
{
    SRAM_P0,
    SRAM_P1,
    SRAM_P2,
    SRAM_P3,
    SRAM_P4,
    SRAM_P5,
    SRAM_VGA,
    SRAM_BOOT,
} SiiSramType_t;
#endif

#define ENABLE_TWO_EDID_MODE  DISABLE

unsigned char Edid_data_all[1][256] = 
{
    {
    //3.1080p,HD,0xAudio,0x7.1;//006
    0x00,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0x00,0x19,0x04,0x06,0x00,0x01,0x00,0x00,0x00,
    0x01,0x17,0x01,0x03,0x80,0x3C,0x22,0x78,0xEA,0x4B,0xB5,0xA7,0x56,0x4B,0xA3,0x25,
    0x0A,0x50,0x54,0xA5,0x4F,0x00,0x81,0x00,0xB3,0x00,0x71,0x4F,0xA9,0x40,0x81,0x80,
    0x01,0x01,0x01,0x01,0x01,0x01,0x02,0x3A,0x80,0x18,0x71,0x38,0x2D,0x40,0x58,0x2C,
    0x25,0x00,0x55,0x50,0x21,0x00,0x00,0x1E,0x02,0x3A,0x80,0xD0,0x72,0x38,0x2D,0x40,
    0x10,0x2C,0x45,0x80,0xC4,0x8E,0x21,0x00,0x00,0x1E,0x00,0x00,0x00,0xFC,0x00,0x30,
    0x30,0x36,0x0A,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x00,0x00,0x00,0xFD,
    0x00,0x38,0x4C,0x1E,0x51,0x11,0x00,0x0A,0x20,0x20,0x20,0x20,0x20,0x20,0x01,0xBC,

    0x02,0x03,0x29,0xF1,0x50,0x90,0x85,0x84,0x83,0x82,0x87,0x96,0x81,0x86,0x91,0x92,
    0x95,0x93,0x94,0x9F,0xA0,0x23,0x5F,0x7F,0x00,0x67,0x03,0x0C,0x00,0x10,0x00,0x38,
    0x2D,0x83,0x01,0x00,0x00,0xE3,0x05,0x03,0x01,0x02,0x3A,0x80,0x18,0x71,0x38,0x2D,
    0x40,0x58,0x2C,0x25,0x00,0x55,0x50,0x21,0x00,0x00,0x1E,0x01,0x1D,0x80,0x18,0x71,
    0x1C,0x16,0x20,0x58,0x2C,0x25,0x00,0x55,0x50,0x21,0x00,0x00,0x9E,0x01,0x1D,0x00,
    0x72,0x51,0xD0,0x1E,0x20,0x6E,0x28,0x55,0x00,0x55,0x50,0x21,0x00,0x00,0x1E,0x8C,
    0x0A,0xD0,0x8A,0x20,0xE0,0x2D,0x10,0x10,0x3E,0x96,0x00,0x55,0x50,0x21,0x00,0x00,
    0x18,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xAB
    },

};


typedef enum
{
    SII_AUD_MCLK_128,       // Fm = 128*Fs
    SII_AUD_MCLK_256,       // Fm = 256*Fs
    SII_AUD_MCLK_384,       // Fm = 384*Fs
    SII_AUD_MCLK_512,       // Fm = 512*Fs
} SiiRxAudioMclk_t;

//------------------------------------------------------------------------------
// Device Component Register initialization list
//------------------------------------------------------------------------------

static unsigned short initRegsList[] =
{
        REG_MISC_CTRL0,             0xCE,
        REG_MISC_CTRL1,             0x0A,
        REG_PAUTH_INV_CTRL,         0x00,       // No clock inversion in pipes and Rx ports
        REG_PA_CONFIG_1,            0x59,       // Enable MHL 1x control
        REG_DPLL_MULTZONE_RDLY1,    0x60,       // Set Rx Zone Control Threshold
        REG_TMDST_TXDAT1,           0x60,       // Set Rx Zone Control Threshold

//        REG_A0_EQ_DATA0,            0x00,
//        REG_A0_EQ_DATA1,            0x20,
//        REG_A0_EQ_DATA2,            0x40,
//        REG_A0_EQ_DATA3,            0x70,
//        REG_A0_EQ_DATA4,            0x23,
//        REG_A0_EQ_DATA5,            0x43,
//        REG_A0_EQ_DATA6,            0x53,
//        REG_A0_EQ_DATA7,            0x20,

        REG_A1_EQ_DATA4,            0x23,      // REG_A1_EQ_DATA0-3 are adjusted dynamically
        REG_A1_EQ_DATA5,            0x43,      // depending on the matrix switch mode enable status
        REG_A1_EQ_DATA6,            0x53,      // in Matrix Switch Driver
        REG_A1_EQ_DATA7,            0x20,

        REG_CLR_PACKET_BUFFER,      ( BIT_VSI_CLR_EN       //Enable to clear the VSI contents
                                    | BIT_VSI_ID_CHK_EN    //Enable VSI IEEE ID check
                                    /*| BIT_USE_AIF_FOR_VSI*/),//Enable the 2nd ID VSI packet uses AIF buffer

        REG_DDC_FILTER_SEL,         0x02,
        REG_SHA_CTRL,               (BIT_SHA_DS_MODE | BIT_SHA_EN), // Enable downstream SHA engine
        REG_TX_OTP_SEL_CTRL,        0x06,   // Assign different OTP AKSVs to Tx0 and Tx1

        //REG_PLL0_CALREFSEL,         0x44,   // Set Rx Zone control reference  //kamal
        REG_PLL0_CALREFSEL,         0x08,   // Set Rx Zone control reference    //kamal
       // REG_PLL0_CALREFSEL,         0x04,
        //REG_PLL1_CALREFSEL,         0x04,//kamal
        REG_PLL1_CALREFSEL,         0x08,//kamal
        //REG_PLL2_CALREFSEL,         0x04,//kamal
        REG_PLL2_CALREFSEL,         0x08,//kamal
        //REG_PLL3_CALREFSEL,         0x04,//kamal
        REG_PLL3_CALREFSEL,         0x08,//kamal
        //REG_PLL4_CALREFSEL,         0x04,//kamal
        REG_PLL4_CALREFSEL,         0x37,//kamal
        REG_PLL5_CALREFSEL,         0x04,
        REG_PLL0_VCOCAL,            0x06,
        REG_PLL1_VCOCAL,			0x06,//kamal
        REG_PLL2_VCOCAL,			0x06,//kamal
        REG_PLL3_VCOCAL,			0x06,//kamal

        REG_TMDS0_CNTL,             0x02,   // new way to set PLL bw at MP
        REG_TMDS0_CNTL2,            0x42,

        REG_A0_EQ_DATA0,            0x04,
        REG_A0_EQ_DATA1,            0x65,
        REG_A0_EQ_DATA2,            0x46,
        REG_A0_EQ_DATA3,            0x36,
        REG_A0_EQ_DATA4,            0x74,
        REG_A0_EQ_DATA5,            0x37,
        REG_A0_EQ_DATA6,            0x75,
        REG_A0_EQ_DATA7,            0x47,

        REG_TMDS0_BW_DATA0,         0x14,   // PLL bandwidth for HDMI at MP
        REG_TMDS0_BW_DATA1,         0x14,
        REG_TMDS0_BW_DATA2,         0x1B,   // PLL bandwidth for MHL at MP
        REG_TMDS0_BW_DATA3,         0x1B,
        REG_TMDS1_BW_DATA0,         0x08,   // PLL bandwidth for HDMI at RP
        REG_TMDS1_BW_DATA2,         0x08,   // PLL bandwidth for MHL at RP
        REG_TMDS1_CNTL,             0x00,   // new way to set PLL bw at RP
#if 0    //#if INC_CBUS
        REG_MHL1X_EQ_DATA0,         0x00,   // MHL 1x EQ table entry0
        REG_MHL1X_EQ_DATA1,         0x20,   // MHL 1x EQ table entry1
        REG_MHL1X_EQ_DATA2,         0x40,   // MHL 1x EQ table entry2
        REG_MHL1X_EQ_DATA3,         0x70,   // MHL 1x EQ table entry3
        REG_PAUTH_NUM_SMPS,         0x85,   // Enable MHL 1x EQ table
#endif

        // RX termination for MHL (ports 0 & 1) and HDMI (ports 1-2) types of inputs
        // NOTE: these settings can depend on value of external termination resistors
        REG_TMDS_TERMCTRL1,         ((VAL_TMDS_TERM_MHL << SFT_TMDS_PORT_EVEN) | VAL_TMDS_TERM_MHL),
        REG_TMDS_TERMCTRL3,         ((VAL_TMDS_TERM_HDMI << SFT_TMDS_PORT_EVEN) | VAL_TMDS_TERM_HDMI),
        REG_XTAL_CAL_CONTRL,        0x37,

        REG_RX_TMDS_TERM_0,         0x00,

        REG_A0_EQ_I2C,              0x20,
        REG_PAUTH_MP_AOVR,          BIT_MP_OVR_P0,
        REG_PAUTH_MPOVR,            0x01,
        REG_SYS_CTRL_1,             0x48,
  //      REG_TMDS2_CCTRL1,           0x37,
        REG_TPI__REG_SEL,           VAL_TPI__REG_SEL__PAGE_L0,
        REG_TPI__REG_OFFSET, 		0x81,
        REG_TPI__REG_DATA,          0xFD,
        REG_TPI__REG_SEL,           VAL_TPI__REG_SEL__PAGE_L0,
        REG_TPI__REG_OFFSET, 		0x80,
        REG_TPI__REG_DATA,          0xA3,
        REG_TPI__REG_SEL,           VAL_TPI__REG_SEL__PAGE_L0,
        REG_TPI__REG_OFFSET, 		0x84,
        REG_TPI__REG_DATA,          0x10,
        REG_TPI__REG_SEL,           VAL_TPI__REG_SEL__PAGE_L0,
        REG_TPI__REG_OFFSET, 		0x83,
        REG_TPI__REG_DATA,          0x88,
        REG_TPI__REG_SEL,           VAL_TPI__REG_SEL__PAGE_L0,
        REG_TPI__REG_OFFSET, 		0x86,
        REG_TPI__REG_DATA,          0x88,
        REG_TPI__REG_SEL,           VAL_TPI__REG_SEL__PAGE_L0,
        REG_TPI__REG_OFFSET, 		0x82,
        REG_TPI__REG_DATA,          0x22,
        REG_TX__TMDS_CTRL2,         0xFD,
        REG_TX__TMDS_CTRL3,         0x3A,//0x23

	REG_TMDS0_CLKDETECT_CTL, 	0x0E,
	REG_TMDS1_CLKDETECT_CTL,	0x0E,//kamal
	REG_TMDS2_CLKDETECT_CTL,	0x0E,//kamal
	REG_TMDS3_CLKDETECT_CTL,	0x0E//kamal
};

typedef enum _SiiGpioPinTypes_t
{
	SII_GPIO_ALT_MHL_CABLE_CONN0    = 0x0001,
    SII_GPIO_ALT_SD1                = 0x0002,       // GPIO_HW_SEL[1] = 0
    SII_GPIO_ALT_SD2                = 0x0004,       // GPIO_HW_SEL[1] = 0
    SII_GPIO_ALT_SD3                = 0x0008,       // GPIO_HW_SEL[1] = 0
    SII_GPIO_ALT_MUTEOUT            = 0x0010,       // GPIO_HW_SEL[2] = 0
    SII_GPIO_ALT_I2S_SCK0           = 0x0020,       // GPIO_HW_SEL[2] = 0
    SII_GPIO_ALT_I2S_WS0            = 0x0040,
    SII_GPIO_ALT_I2S_WS0_OUT        = 0x0080,

    SII_GPIO_ALT_EXT_CLK            = 0x0100,
    SII_GPIO_ALT_SPI_SS             = 0x0100,
    SII_GPIO_ALT_SPI_SCLK           = 0x0200,       // GPIO_HW_SEL[0] = 0
    SII_GPIO_ALT_SPI_SDO            = 0x0400,       // GPIO_HW_SEL[0] = 0
    SII_GPIO_ALT_SPI_SDI            = 0x0800,       // GPIO_HW_SEL[0] = 0
    SII_GPIO_ALT_3D_RP_RIGHT        = 0x4008,       // GPIO_HW_SEL[0] = 1
    SII_GPIO_ALT_3D_RP_ACTIVE       = 0x4010,       // GPIO_HW_SEL[0] = 1
    SII_GPIO_ALT_3D_RP_LEFT         = 0x4020,       // GPIO_HW_SEL[0] = 1
    SII_GPIO_ALT_3D_MP_RIGHT        = 0x4040,       // GPIO_HW_SEL[1] = 1
    SII_GPIO_ALT_3D_MP_ACTIVE       = 0x4080,       // GPIO_HW_SEL[1] = 1
    SII_GPIO_ALT_3D_MP_LEFT         = 0x4100,       // GPIO_HW_SEL[1] = 1
    SII_GPIO_ALT_MP_MUTE            = 0x4200,       // GPIO_HW_SEL[2] = 0
    SII_GPIO_ALT_RP_MUTE            = 0x4400,       // GPIO_HW_SEL[2] = 0

    SII_GPIO_ALTFUNCTION            = 0x4000,
    SII_GPIO_STANDARD               = 0x8000
} SiiGpioPinTypes_t;

typedef enum _SiiGpioPins_t
{
    SII_GPIO_PIN_0		= 0x0001,
    SII_GPIO_PIN_1		= 0x0002,
    SII_GPIO_PIN_2		= 0x0004,
    SII_GPIO_PIN_3		= 0x0008,
    SII_GPIO_PIN_4		= 0x0010,
    SII_GPIO_PIN_5		= 0x0020,
    SII_GPIO_PIN_6		= 0x0040,
    SII_GPIO_PIN_7		= 0x0080,
    SII_GPIO_PIN_8		= 0x0100,
    SII_GPIO_PIN_9		= 0x0200,
    SII_GPIO_PIN_10		= 0x0400,
    SII_GPIO_PIN_11     = 0x8000,
} SiiGpioPins_t;

typedef enum
{
    SII_AUD_TWO_CHANNEL,
    SII_AUD_MULTI_CHANNEL,
} SiiRxAudioLayouts_t;

#if 1
static unsigned short l_pinTypeMask =
    {
    SII_GPIO_ALT_MHL_CABLE_CONN0    |
    SII_GPIO_ALT_EXT_CLK            |
    SII_GPIO_ALT_SPI_SS             |

    SII_GPIO_ALT_SPI_SCLK           |
    SII_GPIO_ALT_SPI_SDO            |
    SII_GPIO_ALT_SPI_SDI            |
    SII_GPIO_ALT_SD1                |
    SII_GPIO_ALT_SD2                |
    SII_GPIO_ALT_SD3                |
    SII_GPIO_ALT_MUTEOUT            |
    SII_GPIO_ALT_I2S_SCK0           |

    SII_GPIO_ALT_I2S_WS0            |

    SII_GPIO_ALT_3D_RP_RIGHT        |
    SII_GPIO_ALT_3D_RP_ACTIVE       |
    SII_GPIO_ALT_3D_RP_LEFT         |
    SII_GPIO_ALT_3D_MP_RIGHT        |
    SII_GPIO_ALT_3D_MP_ACTIVE       |
    SII_GPIO_ALT_3D_MP_LEFT         |
    SII_GPIO_ALT_MP_MUTE            |
    SII_GPIO_ALT_RP_MUTE            |

    0
    };
#endif


extern BYTE I2CBUS ;


void sii_9533_delay(unsigned int ms)
{
    unsigned int i=0, j=0;

	for(i=0;i<110;i++)
	{
        for(j=0;j<ms;j++)
		{

		}
	}
}


//------------------------------------------------------------------------------------------------------------------------//


//-------------------------------------------------------------------------------------------------
//! @brief      Read a one byte register.
//! @param[in]  virtualAddress  - Sixteen bit virtual register address, including device page.
//! @return     eight bit register data.
//-------------------------------------------------------------------------------------------------
unsigned char SiiRegRead (unsigned short virtualAddr )
{
    unsigned char value = 0;

	I2CBUS = SiI_I2CBUS ;
	value = hlReadByte_8BA( l_regioDecodePage[virtualAddr >> 8], (virtualAddr&0xFF));

    return( value );
}

//-------------------------------------------------------------------------------------------------
//! @brief      Write a one byte register.
//! @param[in]  virtualAddress  - Sixteen bit virtual register address, including device page.
//! @param[in]  value           - eight bit data to write to register.
//! @return     None
//-------------------------------------------------------------------------------------------------
void SiiRegWrite (unsigned short virtualAddr, unsigned char value )
{
	I2CBUS = SiI_I2CBUS ;
    hlWriteByte_8BA(l_regioDecodePage[virtualAddr >> 8], (virtualAddr&0xFF), value );
}

//------------------------------------------------------------------------------
// Function:    SiiRegModify
// Description: Reads the register, performs an AND function on the data using
//              the mask parameter, and an OR function on the data using the
//              value ANDed with the mask. The result is then written to the
//              device register specified in the regAddr parameter.
// Parameters:  regAddr - Sixteen bit register address, including device page.
//              mask    - Eight bit mask
//              value   - Eight bit data to be written, combined with mask.
// Returns:     None
//------------------------------------------------------------------------------
void SiiRegModify (unsigned short virtualAddr, unsigned char mask, unsigned char value)
{
    unsigned char aByte;

    aByte = SiiRegRead( virtualAddr );
    aByte &= (~mask);                       // first clear all bits in mask
    aByte |= (mask & value);                // then set bits from value
    SiiRegWrite( virtualAddr, aByte );
}

//------------------------------------------------------------------------------
// Function:    SiiRegBitsSet
// Description: Reads the register, sets the passed bits, and writes the
//              result back to the register.  All other bits are left untouched
// Parameters:  regAddr - Sixteen bit register address, including device page.
//              bits   - bit data to be written
// Returns:     None
//------------------------------------------------------------------------------
void SiiRegBitsSet ( unsigned short virtualAddr, unsigned char bitMask, unsigned char setBits )
{
    unsigned char aByte;

    aByte = SiiRegRead( virtualAddr );
    aByte = (setBits) ? (aByte | bitMask) : (aByte & ~bitMask);
    SiiRegWrite( virtualAddr, aByte );
}

//------------------------------------------------------------------------------
// Function:    SiiRegBitsSetNew
// Description: Reads the register, sets or clears the specified bits, and
//              writes the result back to the register ONLY if it would change
//              the current register contents.
// Parameters:  regAddr - Sixteen bit register address, including device page.
//              bits   - bit data to be written
//              setBits- true == set, false == clear
// Returns:     None
//------------------------------------------------------------------------------
void SiiRegBitsSetNew (unsigned short virtualAddr, unsigned char bitMask, unsigned char setBits )
{
    unsigned char newByte, oldByte;

    oldByte = SiiRegRead( virtualAddr );
    newByte = (setBits) ? (oldByte | bitMask) : (oldByte & ~bitMask);
    if ( oldByte != newByte )
    {
        SiiRegWrite( virtualAddr, newByte );
    }
}


//------------------------------------------------------------------------------------------------------------------------//


//------------------------------------------------------------------------------
// Function:    SiiDrvDeviceIdGet
// Description: This function returns the chip ID.
// Parameters:  none
// Returns:     I16 bit chip id.
//------------------------------------------------------------------------------
unsigned int SiiDrvDeviceIdGet(void)
{
	unsigned int devType;

    devType = SiiRegRead( REG_DEV_IDH_RX );
    devType = ( devType << 8) | SiiRegRead( REG_DEV_IDL_RX );

    printf("   SiI_9533 dev_id = %x \r\n", (unsigned int)devType);	 // dev_id=0x9533

    return devType;
}

//------------------------------------------------------------------------------
// Function:    SiiDrvDeviceRevGet
// Description: This function returns the chip ID.
// Parameters:  none
// Returns:     I16 bit chip id.
//------------------------------------------------------------------------------
unsigned char SiiDrvDeviceRevGet(void)
{
    unsigned char devRev = 0;
	 
	devRev = SiiRegRead( REG_DEV_REV );
	printf("   SiI_9533 dev_rev = %x \r\n", (unsigned int)devRev);	 //	dev_rev=0x10
    return  SiiRegRead( REG_DEV_REV );
}

// 读取TPI模块的ID、版本号等信息，必须在使能TPI mode 之后（0x72:0xC7 <- 0x00）,	否则不能正确读取ID等数据
void SiiDrvTpiDeviceIdGet(void)
{
    unsigned char deviceId=0;
    unsigned char deviceRevision=0;
	unsigned char tpiRev=0;

    deviceId = SiiRegRead(REG_TPI__DEVICE_ID);
	printf("   SiI9533 TPI_DEV_ID : %x  \r\n", (unsigned int)deviceId);	        // TPI_DEV_ID = 0xB6
    deviceRevision = SiiRegRead(REG_TPI__DEVICE_REV_ID);
	printf("   SiI9533 TPI_DEV_REV_ID : %x  \r\n", (unsigned int)deviceRevision);  // TPI_DEV_REV_ID = ?
	tpiRev = SiiRegRead(REG_TPI__TPI_REV_ID) & 0x7F;
	printf("   SiI9533 TPI_REV_ID : %x  \r\n", (unsigned int)tpiRev);              // TPI_REV_ID =?

	printf("   --- If TPI_DEV_ID != 0xB6(different from datasheet), please check 0x72:0xc0(0x72:0xc0 0x00)  \r\n");  
}

//------------------------------------------------------------------------------------------------------------------------//



#if 0
void SiiDrvNvramNonVolatileWrite ( void )
{
	//DEBUG_PRINT( MSG_ERR, "\nNVRAM: In SiiDrvNvramNonVolatileWrite() \n" );
    // Load the source data into the appropriate SRAM

	//DEBUG_PRINT( MSG_ERR, "\nNVRAM: In SiiDrvNvramSramWrite() \n" );
	// Point to offset into selected port SRAM.
	SiiRegModify( REG_EDID_FIFO_SEL, MSK_SEL_EDID_FIFO | BIT_SEL_DEVBOOT, 0 );
	SiiRegWrite( REG_EDID_FIFO_ADDR, 0 );
	
	// Write data into the destination.
	////SiiRegWriteBlock( REG_EDID_FIFO_DATA, pSrc, length );

 
	// DEBUG_PRINT( MSG_ERR, "\nNVRAM: In SiiDrvNvramNonVolatileWrite()-> SRAM WRITE SUCESS \n" );
	// Start the NVRAM program operation and wait for it to finish.
    // Enable Replacing of BSM registers with alternate values
    SiiRegWrite( REG_NVM_BSM_REPLACE, 0x01 );

    SiiRegBitsSet( REG_SYS_RESET_2, BIT_NVM_SRST, 1 );      // NVM Soft Reset
    SiiRegBitsSet( REG_SYS_RESET_2, BIT_NVM_SRST, 0 );      // Release NVM soft reset

    SiiRegWrite( REG_NVM_COMMAND, 0x04 );     // Start the NVRAM program operation	

	// Enable Replcing of BSM registers with alternate values
	SiiRegWrite( REG_NVM_BSM_REPLACE, 0x01 );
	// Write Second Byte of Boot Data
	SiiRegWrite( REG_NVM_REPLACE_BYTE1, 0x8C);//(pSrc[1] | 0x80)

}
#endif


//-------------------------------------------------------------------------------------------------
//! @brief      Writes a block of data to sequential registers.
//! @param[in]  virtualAddress  - Sixteen bit virtual register address, including device page.
//! @param[in]  pBuffer     - source data buffer.
//! @param[in]  count       - number of registers (bytes) to write.
//! @return     none
//! @note       This function relies on the auto-increment model used by
//! @note       Silicon Image devices.  Because of this, if a FIFO register
//! @note       is encountered before the end of the requested count, the
//! @note       data remaining from the count is written to the FIFO, NOT
//! @note       to subsequent registers.
//-------------------------------------------------------------------------------------------------
void SiiRegWriteBlock ( unsigned short virtualAddr, const unsigned char *pBuffer, unsigned int count )
{
    unsigned int i=0;

	for(i=0;i<count;i++)
	{
	    SiiRegWrite (virtualAddr+i, pBuffer+i );
	}
}



//-------------------------------------------------------------------------------------------------
//! @brief      Write data to the requested SRAM.
//  Parameters:
//!
//! @retval     - true: successful.
//! @retval     - false: failure
//-------------------------------------------------------------------------------------------------

unsigned char SiiDrvNvramSramWrite ( SiiSramType_t sramType, const unsigned char *pSrc, int offset, int length )
{
    unsigned char ramSelect = 0;
	unsigned char flag1=0;

    flag1 = 0;
    switch (sramType)
    {
        case SRAM_P0:
        case SRAM_P1:
        case SRAM_P2:
        case SRAM_P3:
        case SRAM_P4:
        case SRAM_P5:
        case SRAM_VGA:
            ramSelect = (unsigned char)sramType;
            break;
        case SRAM_BOOT:
            ramSelect = BIT_SEL_DEVBOOT;
            break;

        default:
            flag1 = 1;
            break;
    }

    if ( flag1 == 0 )
    {
    	//DEBUG_PRINT( MSG_ERR, "\nNVRAM: In SiiDrvNvramSramWrite() \n" );
        // Point to offset into selected port SRAM.
        SiiRegModify( REG_EDID_FIFO_SEL, MSK_SEL_EDID_FIFO | BIT_SEL_DEVBOOT, ramSelect );
        SiiRegWrite( REG_EDID_FIFO_ADDR, offset );

        // Write data into the destination.
        SiiRegWriteBlock( REG_EDID_FIFO_DATA, pSrc, length );
    }

    return( flag1 == 0 );
}


//-------------------------------------------------------------------------------------------------
//! @brief      Execute the passed NVRAM command.  Does not wait for command to complete.
//!
//! @param[in]  command - NVRAM command
//-------------------------------------------------------------------------------------------------
static unsigned char SendNvramCommand ( unsigned char command, unsigned char isSynchronous )
{
    unsigned int   count=0;
    unsigned char     test;

    // Enable Replacing of BSM registers with alternate values
    SiiRegWrite( REG_NVM_BSM_REPLACE, 0x01 );

    SiiRegBitsSet( REG_SYS_RESET_2, BIT_NVM_SRST, 1 );      // NVM Soft Reset
    SiiRegBitsSet( REG_SYS_RESET_2, BIT_NVM_SRST, 0 );     // Release NVM soft reset

    SiiRegWrite( REG_NVM_COMMAND, command );    // Start the NVRAM program operation
    if ( isSynchronous )
    {
       // Set 4 second timeout
        for ( ;; )
        {
		    count++; 
            test = SiiRegRead( REG_NVM_COMMAND_DONE );
            if ( test & BIT_NVM_COMMAND_DONE )
                break;
			if(count >4000)
			   break;

        }
    }

    return( 1 );
}


//-------------------------------------------------------------------------------------------------
//! @brief      Start a program operation of the specified type on the NVRAM
//! @param[in]  nvramType - NVRAM_HDMI_EDID, specifies main NVRAM
//!                         NVRAM_VGA_EDID, specifies VGA EDID
//!                         NVRAM_BOOTDATA, specifies Boot Data
//!
//! @retval     - true: successful.
//! @retval     - false: failure
//-------------------------------------------------------------------------------------------------
void SiiDrvNvramProgram ( SiiNvramType_t nvramType, unsigned char isSynchronous )
{
    unsigned char nvramCommand = 0;
	unsigned char flag2=0;

    flag2 = 0;
    switch ( nvramType )
    {
        case NVRAM_HDMI_EDID:
            nvramCommand    = VAL_PRG_EDID;
            break;

        case NVRAM_VGA_EDID:
            nvramCommand    = VAL_PRG_VGA;
            break;

        case NVRAM_BOOTDATA:
            nvramCommand    = VAL_PRG_DEVBOOT;
            break;
        default:
            flag2 = 1;
            break;
    }

    if ( flag2 == 0 )
    {
    	//DEBUG_PRINT( MSG_ERR, "\nNVRAM: In SiiDrvNvramProgram()-- 1 \n" );
        if ( !SendNvramCommand( nvramCommand, isSynchronous ))
        {
            //pDrvNvram->lastResultCode = SII_DRV_NVRAM_ERR_FAIL;
           // DEBUG_PRINT( MSG_ERR, "\nNVRAM: In SiiDrvNvramProgram()--FAILED Here \n" );
        }
        else
        {
        	//DEBUG_PRINT( MSG_ERR, "\nNVRAM: In SiiDrvNvramProgram()--SUCCESS Here \n" );
        }
    }

    //return( pDrvNvram->lastResultCode == SII_DRV_NVRAM_SUCCESS );
}



#if 0
//-------------------------------------------------------------------------------------------------
//! @brief      Program the passed data into the NVRAM of the specified TYPE
//! @param[in]  nvramType - NVRAM_HDMI_EDID, specifies main NVRAM
//!                         NVRAM_VGA_EDID, specifies VGA EDID
//!                         NVRAM_BOOTDATA, specifies Boot Data
//!
//! @retval     - true: successful.
//! @retval     - false: failure
//-------------------------------------------------------------------------------------------------
void SiiDrvNvramNonVolatileWrite ( SiiNvramType_t nvramType, const unsigned char *pSrc, int offset, int length )
{
    unsigned char flag=0;
    SiiSramType_t   sramType = 0;

    flag = 0;

    switch ( nvramType )
    {
        case NVRAM_HDMI_EDID:
            sramType        = SRAM_P0;
            break;
        case NVRAM_VGA_EDID:
            sramType        = SRAM_P0;
            break;
        case NVRAM_BOOTDATA:
            sramType        = SRAM_BOOT;
            break;
        default:
            flag = 1;
            break;
    }

    if ( flag == 0 )
    {
    	//DEBUG_PRINT( MSG_ERR, "\nNVRAM: In SiiDrvNvramNonVolatileWrite() \n" );
        // Load the source data into the appropriate SRAM
        if ( SiiDrvNvramSramWrite( sramType, pSrc, offset, length ))
        {
        //	DEBUG_PRINT( MSG_ERR, "\nNVRAM: In SiiDrvNvramNonVolatileWrite()-> SRAM WRITE SUCESS \n" );
            // Start the NVRAM program operation and wait for it to finish.
            SiiDrvNvramProgram( nvramType, 1 );

        	if(nvramType == NVRAM_BOOTDATA)
        	{
        		// Enable Replcing of BSM registers with alternate values
        		SiiRegWrite( REG_NVM_BSM_REPLACE, 0x01 );
        		// Write Second Byte of Boot Data
        		SiiRegWrite( REG_NVM_REPLACE_BYTE1, (/*pSrc[1] | */0x8C) );//(pSrc[1] | 0x80)
        	}
        }
    }

}
#endif




//------------------------------------------------------------------------------
// Function:    SiiDrvDevicePowerUpBoot
// Description: Perform the device startup sequence.  Reset, wait for boot to
//				complete, optionally initialize EDID NVRAM, and initialize
//				device registers.
// Parameters:  none
// Returns:     true if device booted and initialized correctly
//------------------------------------------------------------------------------
void SiiDrvDevicePowerUpBoot(void)
{
    unsigned char test=0;
	unsigned int devTypex = 0;

	test = SiiRegRead( REG_BSM_STAT );
	//sii_9533_delay(50);                          // Wait for boot loading to be done.  
	//test = SiiRegRead( REG_BSM_STAT );
	if(test & BIT_BOOT_DONE )
	{
	    printf("   NVRAM BOOT DONE status ok \r\n");
	}
	else 
	{
		printf("   NVRAM BOOT DONE status error \r\n");
	}

	// Force a soft Hard Reset to ensure that the Always On Domain 
	// logic is reset.  This is needed in case this is NOT a cold 
	// power-up, but a resume from standby or a cold power-up when 
	// one or more powered sources are connected.
#if 0	//注意：这里不能有这两行代码
	SiiRegWrite( REG_SPECIAL_PURPOSE, BIT_HARDRESET );
	(void) SiiRegRead( REG_SPECIAL_PURPOSE );  // Dummy read to clear write interrupted by reset
#endif

	sii_9533_delay(50);

	//SWWA: FP1557
	// The audio PLLs may not have been properly powered up if the
	// 3.3v and 1.3v power supplies were improperly sequenced, so
	// turn them off and on to ensure proper operation.
	SiiRegWrite( REG_APLL_POWER, 0x00);
	sii_9533_delay(50);
	SiiRegWrite( REG_APLL_POWER, BIT_APLL0_PWR_ON | BIT_APLL1_PWR_ON);

	
	if(( SiiRegRead( REG_NVM_STAT ) != VAL_NVM_VALID ) || 0 )
	{
	    printf("   NVRAM has been initialized ok \r\n");

#if 0
	    SiiDrvNvramNonVolatileWrite( NVRAM_BOOTDATA, (unsigned char *)&gEdidFlashDevBootData, 0, EDID_DEVBOOT_LEN );

		SiiDrvNvramNonVolatileWrite( NVRAM_HDMI_EDID, (unsigned char *)&gEdidFlashEdidTable, 0, EDID_TABLE_LEN );
        SiiDrvNvramNonVolatileWrite( NVRAM_HDMI_EDID, (unsigned char *)&gEdidFlashEdidTable, 256, EDID_TABLE_LEN );
        SiiDrvNvramNonVolatileWrite( NVRAM_HDMI_EDID, (unsigned char *)&gEdidFlashEdidTable, 512, EDID_TABLE_LEN );
        SiiDrvNvramNonVolatileWrite( NVRAM_HDMI_EDID, (unsigned char *)&gEdidFlashEdidTable, 768, EDID_TABLE_LEN );

		// Force a boot load to get the new EDID data from the NVRAM to the chip.
		SiiRegWrite( REG_BSM_INIT, BIT_BSM_INIT );

		test = SiiRegRead( REG_BSM_STAT );
		//sii_9533_delay(50);                 // Wait for boot loading to be done.  
		//test = SiiRegRead( REG_BSM_STAT );
		if(test & BIT_BOOT_DONE )
		{
			printf("   NVRAM BOOT1 DONE status ok \r\n");
		}
		else 
		{
			printf("   NVRAM BOOT1 DONE status error \r\n");
		}
#endif
	    
	}
	else
	{
	    printf("   NVRAM has been initialized error \r\n");
	}
}


//------------------------------------------------------------------------------
// Function:    DrvDeviceInitRegisters
// Description: Initialize registers that need to be set to non-default values
//              at startup.  In general, these registers are not changed
//              after startup.
// Parameters:  none
// Returns:     none
//-----------------------------------------------------------------------------c

static void DrvDeviceInitRegisters(void)
{
    unsigned int index;

#if 0
	SiiRegBitsSet(0x0008, 0x04, 0); // Independent pipes have different BKSVs// wyq---20150615
#endif

    // Turn off port change logic and hold device in
    // software reset until finished update.
	printf("   SiI9533 system reset \r\n");
    SiiRegModify( REG_MISC_CTRL1, BIT_PORT_CHG_ENABLE, CLEAR_BITS );
    SiiRegModify( REG_SYS_RESET_1, BIT_SWRST, SET_BITS );

    SiiRegModify( REG_SYS_RESET_3, BIT_HDCP_RST, SET_BITS   ); // HDCP arbitration and OTP reset
	sii_9533_delay(50);
    SiiRegModify( REG_SYS_RESET_3, BIT_HDCP_RST, CLEAR_BITS ); // Release


    /* Perform the majority of the required register initialization.    */
	//printf("edid num=%d \r\n",sizeof( initRegsList) );
	//for ( index = 0; index < 146; index += 2 )
    for ( index = 0; index < (sizeof( initRegsList) / 2); index += 2 )	
    {
        SiiRegWrite( initRegsList[ index], initRegsList[ index + 1] );
    }

    SiiRegWrite( REG_HPE_HW_CTRL_OVR, MSK_INVALIDATE_HW_HPD_CTRL ); // Disable Auto-HPD control
	//SiiRegWrite( REG_HPE_HW_CTRL_OVR, 0x00 ); // Disable Auto-HPD control

#if 1
    // NOTE: Caller must call SiiDrvDeviceRelease() function to bring out of reset
    SiiRegModify( REG_PD_SYS_EXT2, 0xff, 0x0f );
    SiiRegModify( REG_PD_SYS4, 0xff, 0x0f );// TMDS Clock Channel power mode
#endif
}

//-------------------------------------------------------------------------------------------------
//! @brief      Initialize both instances of the Matrix Switch
//-------------------------------------------------------------------------------------------------
void SkAppDeviceInitMsw( void )
{
	// Clear the hardware Auto-HPD mask to default (All HDMI),
	SiiRegModify( REG_PHYS_HPD_DISABLE, VAL_FW_HPE_MASK, CLEAR_BITS );
	
	// Set the end point for the EDID DDC disable timer to 1200ms
	SiiRegWrite( REG_IP_HPE_EDID_DDC_END, 0x0C );
	
	// Enable the interrupts for this driver
	SiiRegBitsSet( REG_INT_ENABLE_P3, BIT_P3_ENABLE_MP_NEW_ALL | BIT_P3_ENABLE_MP_NO_ALL, 1); // MP
	SiiRegBitsSet( REG_INT_ENABLE_P4, BIT_RP_NEW_AVI, 0); // RP: disabled by default
	SiiRegBitsSet( REG_INT_ENABLE_5, 0xFF, 1);
	
	// Enable both Tx0 and Tx1 by default
	SiiRegWrite( REG_TX_VIDEO_SRC, BIT_TX0_PIPE_EN);
	//SiiRegWrite( REG_TX_VIDEO_SRC, 0x00);	//wyq---20150616
			
	// Enable Video clock counter
	SiiRegBitsSet(REG_XPCLK_ENABLE, BIT_XPCLK_ENABLE, SET_BITS);
}


void SiiDrvDeviceRelease ( void )
{
#if 0
    SiiRegWrite(REG_HDCP_KSV_FIFO_CTRL, VAL_HDCP_FIFO_VALID);

	//SiiRegModify(REG_HDCP_BCAPS_SET, 0x03 << (inputPort[pipeNumber] * 2), CLEAR_BITS); //both repeater and FIFO rdy
	//SiiRegWrite(REG_HDCP_BCAPS_SET, VAL_P3_REPEATER | VAL_P2_REPEATER | VAL_P1_REPEATER | VAL_P0_REPEATER);
	SiiRegWrite(REG_HDCP_BCAPS_SET, CLEAR_BITS);

	SiiRegModify(REG_HDCP_KSV_FIFO_CTRL, VAL_HDCP_FIFO_VALID, SET_BITS);
#endif

    // Release software reset and finite state machine. Must delay between the two events.
    SiiRegModify( REG_SYS_RESET_1, BIT_SWRST, CLEAR_BITS );
    sii_9533_delay(50); 
    SiiRegModify( REG_MISC_CTRL1, BIT_PORT_CHG_ENABLE, SET_BITS );
}


//-------------------------------------------------------------------------------------------------
//  TX DDC Access API functions
//-------------------------------------------------------------------------------------------------

//-------------------------------------------------------------------------------------------------
//! @brief      Initialization of DDC access module.
//-------------------------------------------------------------------------------------------------

void SiiDrvTpiDdcInit(void)
{
    // DDC speed formula: F_ddc = 1/(200ns * (4 * (delay count + 1)  + 6))
    SiiRegWrite(REG_TPI__DDC_DELAY_CNT, 0x0E); // Set DDC Speed ~ 76 kHz
	//SiiRegWrite(REG_TPI__DDC_DELAY_CNT, 0x22); // Set DDC Speed ~ 76 kHz   wyq---20150616
	sii_9533_delay(50); 
}


//-------------------------------------------------------------------------------------------------
//! @brief      Initialize TPI module
//-------------------------------------------------------------------------------------------------

void SiiDrvTpiInit(void)
{
    SiiDrvTpiDdcInit();

#if 0
	SiiRegWrite(REG_TPI__REG_SEL,0x01);
	SiiRegWrite(REG_TPI__REG_OFFSET, 0xF9);
	SiiRegWrite(REG_TPI__REG_DATA, 0);
#endif

    // ---------------- Inits & SWWAs -------------------

    // TMDS termination resistor to support Vclk up to 165MHz
    // The differential termination
    //SiiDrvTpiIndirectWrite(REG_TX__TMDS_TERM_SELECT, VAL_TX__TMDS_TERM__NONE);
    // TMDS Swing adjustment per chip characterization
    //SiiDrvTpiIndirectWrite(REG_TX__TMDS_SWING_CTRL, 0xDB);
    //SiiDrvTpiIndirectWrite(REG_TX__TMDS_PLL_BW, 0x04);

    // No HDCP protection on the link
    // Enable HDCP cancellation whenever HDCP engine stopped
    SiiRegWrite(REG_TPI__HDCP_CTRL, BIT_TPI__HDCP_CTRL__PROT_REQUEST_CANCEL);
	printf("   --- Tx HDCP protection disenable(0x72:0x2A 0x40) \r\n");

#if 1
    // TMDS output power down, output mode - DVI
    SiiRegWrite(REG_TPI__SYSTEM_CONTROL, BIT_TPI__SYSTEM_CONTROL__PD);
	printf("   --- TMDS output power down(0x72:0x1A 0x10) \r\n");
#endif


#if 0
    // Check if DS device is connected, then simulate HPD interrupt catch
    if ((SiiRegRead(REG_TPI__INT_STATUS1) & BIT_TPI__INT__HPD_STATE) != 0)
    {
        printf("   DS device is ok  \r\n");
    }
	else
	{
	    printf("   DS device is error  \r\n");
	}
#endif

#if 1
	// Initial value for R0 calculation time.
	// Will be updated based on the incoming infoframe
	SiiRegWrite(REG_TPI__HW_OPT, VAL_TPI__R0_CALC_OSC);
#endif
}


//-------------------------------------------------------------------------------------------------
//! @brief      Initialization of the HDMI transmitter
//!
//!             Initializes Tx module and sets default parameters of the component.
//!             This function shall be called once and foremost.
//-------------------------------------------------------------------------------------------------
void SiiTxInitialize(void)
{
    unsigned char regVal=0;

	// Software reset and initialization of Tx to bring it to a known initial state

	// Enable TPI access mode for normal operation
	printf("   SiI9533 Enable hardware TPI mode(0x72:0xc7 0x00)   -Important- \r\n");
    SiiRegWrite(REG_TX_TPI__ENABLE, BIT_TX_TPI__ENABLE);	// 0x72:0xc7   0x00
	//SiiRegWrite(0x10c7, 0x00);	// 0x72:0xc7   0x00

	sii_9533_delay(50); 

    // --------------------------TPI software reset
#if 0
	regVal = SiiRegRead(REG_TPI__RAR);
    regVal |= BIT_TPI__RAR__SW_RST;
    SiiRegWrite(REG_TPI__RAR, regVal);
    regVal &= ~BIT_TPI__RAR__SW_RST;
    SiiRegWrite(REG_TPI__RAR, regVal);

	sii_9533_delay(50);    // Wait 20 ms to settle down after software reset
#endif

	// 读取 TPI 模块ID、版本号等信息，若读取的数据与datasheet不同，则说明 TPI mode 没有正确使能
	SiiDrvTpiDeviceIdGet();

    SiiDrvTpiInit();

	SiiRegBitsSet(REG_TPI__INT_ENABLE1, TX_INTERRUPT_MASK0, 1 );
    SiiRegBitsSet(REG_TPI__INT_ENABLE2, TX_INTERRUPT_MASK1, 1 );
}

void SiiDrvTpiIndirectBlockWrite(unsigned short regAddr, unsigned char *pData, unsigned char length)
{
    unsigned char page    = 0x01;
    unsigned char offset  = regAddr & 0xFF;

    //SetIndirectLegacyPage(page);
	SiiRegWrite(REG_TPI__REG_SEL, page & MSK_TPI__REG_SEL__PAGE);
    for (; length; length--)
    {
        //SetIndirectLegacyOffset(offset);
		SiiRegWrite(REG_TPI__REG_OFFSET, offset);

        SiiRegWrite(REG_TPI__REG_DATA, *pData);
        pData++;
        offset++;
    }
}


//-------------------------------------------------------------------------------------------------
//! @brief      Enable or Disable TMDS output signals.
//!
//! @param[in]  isTmdsEnabled - true enables TMDS, false disables TMDS
//-------------------------------------------------------------------------------------------------

void SiiDrvTpiTmdsOutputEnable(unsigned char isEnabled)
{
	if(isEnabled)
	{
		// Fix to RTL BUG (26110): clear "bgr_ext_res_en" bit-1 of 0x83 regester in Legacy Tx page 0
		//SiiDrvTpiIndirectWrite(REG_TX__AUDIO_CLK_DIV, 0x88);
		SiiDrvTpiIndirectBlockWrite(REG_TX__AUDIO_CLK_DIV, 0x88, 1)	;
	}

    SiiRegModify(REG_TPI__SYSTEM_CONTROL, BIT_TPI__SYSTEM_CONTROL__PD, isEnabled ? CLEAR_BITS : SET_BITS);
}

//-------------------------------------------------------------------------------------------------
//! @brief      Set an output mode (DVI or HDMI).
//!
//! @param[in]  isHdmi - true for HDMI, false for DVI
//-------------------------------------------------------------------------------------------------

void SiiDrvTpiHdmiOutputModeSet(unsigned char isHdmi)
{
    SiiRegModify(REG_TPI__SYSTEM_CONTROL, BIT_TPI__SYSTEM_CONTROL__HDMI,
                            isHdmi ? SET_BITS : CLEAR_BITS);
}

//-------------------------------------------------------------------------------------------------
//! @brief      Standby mode.
//-------------------------------------------------------------------------------------------------

void SiiDrvTpiStandby(void)
{
    // Disable interrupts for the TPI driver
    SiiRegBitsSet(REG_TPI__INT_ENABLE1, TX_INTERRUPT_MASK0, 0 );
    SiiRegBitsSet(REG_TPI__INT_ENABLE2, TX_INTERRUPT_MASK1, 0 );

    // Clear outstanding interrupts
    SiiRegWrite( REG_TPI__INT_ENABLE1, TX_INTERRUPT_MASK0 );
    SiiRegWrite( REG_TPI__INT_ENABLE2, TX_INTERRUPT_MASK1 );

    // Go into low power state (D2)
    SiiRegWrite(REG_TPI__DEVICE_POWER_STATE_CTRL, VAL_TPI__POWER_STATE_D2);
	
}

//-------------------------------------------------------------------------------------------------
//! @brief      Turn power on.
//-------------------------------------------------------------------------------------------------
void SiiDrvTpiPowerUp(void)
{
    SiiRegWrite(REG_TPI__DEVICE_POWER_STATE_CTRL, VAL_TPI__POWER_STATE_D0);	   // 0x72:0x1E 0x00
	printf("   TPI Transmitter power state(0x72:0x1E 0x00),hear is D0 state \r\n");
}

//-------------------------------------------------------------------------------------------------
//! @brief      Enable/Disable HDCP protection.
//!
//! @param[in]  isEnabled - true, if HDCP protection has to be enabled.
//-------------------------------------------------------------------------------------------------
void SiiDrvTpiHdcpProtectionEnable(unsigned char isEnabled)
{
    SiiRegModify(REG_TPI__HDCP_CTRL, HDCP_CTRL_MODE, isEnabled ? SET_BITS : CLEAR_BITS);
}




//-------------------------------------------------------------------------------------------------
//! @brief      Initialization of TX HDCP module.
//-------------------------------------------------------------------------------------------------

void TxHdcpInit(void)
{
    SiiRegModify(REG_TPI__HDCP_CTRL, BIT_TPI__HDCP_CTRL__ENCRYPT_DISABLE, SET_BITS);
	//SiiRegModify(REG_TPI__SYSTEM_CONTROL, BIT_TPI__SYSTEM_CONTROL__AVMUTE, SET_BITS );
    SiiRegModify(REG_TPI__HDCP_CTRL, HDCP_CTRL_MODE, CLEAR_BITS);

	SiiRegModify(REG_TPI__HDCP_CTRL, HDCP_CTRL_MODE, CLEAR_BITS);

    //SiiRegModify(REG_TPI__HDCP_CTRL, BIT_TPI__HDCP_CTRL__ENCRYPT_DISABLE, SET_BITS);

    SiiDrvTpiHdcpProtectionEnable(0);
      
#if 0    
	if((SiiRegRead(REG_TPI__HDCP_QUERY) & BIT_TPI__HDCP_QUERY__SINK_CAPABLE) != 0)
	{
	    printf("HDCP is ok \r\n");
		   
	}
	else
	{
	    printf("HDCP is error \r\n"); 
	}
#endif

    SiiRegModify(REG_TPI__SYSTEM_CONTROL, BIT_TPI__SYSTEM_CONTROL__REAUTH_EN, SET_BITS );
}

//-------------------------------------------------------------------------------------------------
//! @brief      Set pass through mode.
//!
//! @param[in]  isAudioPassEnabled     - Audio packets will pass through, if true
//! @param[in]  isNonAudioPassEnabled  - Non-audio packets will pass through, if true
//-------------------------------------------------------------------------------------------------

void SiiDrvRxAudioMixPassThroughConfig(unsigned char isAudioPassEnabled, unsigned char isNonAudioPassEnabled)
{
	//SiiRegModify( REG_TPI__AUDIO_CFG, (BIT_TPI__AUDIO_CFG__MUTE | VAL_TPI__AUDIO_CFG__SPDIF | VAL_TPI__AUDIO_CFG__I2S), BIT_TPI__AUDIO_CFG__MUTE);
    SiiRegModify( REG_TX_AUD_MIX_CTRL1, BIT_TX_AUD_MIX_PASS_AUD_PKT |
                  BIT_TX_AUD_MIX_PASS_NAUD_PKTS | BIT_TX_AUD_MIX_DROP_GEN1,
                    (isAudioPassEnabled ? BIT_TX_AUD_MIX_PASS_AUD_PKT : 0) |
                    (isNonAudioPassEnabled ? BIT_TX_AUD_MIX_PASS_NAUD_PKTS : 0) |
                    /* SWWA: 23624 Always drop NULL packets. They may get in the way of mixer operation */
                    BIT_TX_AUD_MIX_DROP_GEN1);

    SiiRegModify( REG_TX_AUD_MIX_CTRL0, BIT_TX_AUD_MIX_DROP_AIF | BIT_TX_AUD_MIX_DROP_SPDIF | BIT_TX_AUD_MIX_DROP_CTS,
                    (isAudioPassEnabled ? 0: (BIT_TX_AUD_MIX_DROP_AIF | BIT_TX_AUD_MIX_DROP_SPDIF | BIT_TX_AUD_MIX_DROP_CTS)) );

    SiiRegModify( REG_TX_AUD_MIX_CTRL3, BIT_TX_AUD_MIX_AUD_TYPE_OVR, (isAudioPassEnabled ? 0: BIT_TX_AUD_MIX_AUD_TYPE_OVR));
}

//-------------------------------------------------------------------------------------------------
//! @brief      drop GCP packet.
//!
//! @param[in]
//-------------------------------------------------------------------------------------------------

void SiiDrvRxAudioMixGcpPassThroughConfig(unsigned char qOn)
{
    SiiRegModify( REG_TX_AUD_MIX_CTRL0, BIT_TX_AUD_MIX_DROP_GCP, (qOn ? 0: BIT_TX_AUD_MIX_DROP_GCP) );
}



//-------------------------------------------------------------------------------------------------
//! @brief      Initialization of Info Frame module.
//-------------------------------------------------------------------------------------------------

void TxInfoInit(void)
{
    unsigned char selector=0;

	for (selector = 0; selector <= 7; selector++)
	{
		SiiRegWrite(REG_TPI__IF_SELECT, selector);
	}
}


void SkAppDeviceInitTx(void)
{   
	unsigned char selector=0;

	//--------------------------------------------------------------------------------------------//
	SiiTxInitialize();

#if 1
	SiiDrvTpiTmdsOutputEnable(0);

	SiiDrvTpiStandby();
#endif

#if 0
	SiiDrvTpiPowerUp();
#endif

#if 1
    TxInfoInit();
#endif

	//SiiTxAvUnmute();
	SiiRegModify(REG_TPI__SYSTEM_CONTROL, BIT_TPI__SYSTEM_CONTROL__AVMUTE, CLEAR_BITS);

#if 1
	TxHdcpInit();
#endif

	//--------------------------------------------------------------------------------------------//

	// Configure Audio Mixer to bypass audio and other packets
	SiiDrvRxAudioMixPassThroughConfig(1, 1);	//配置Tx音频（非音频通道）

	// GCP packets shall never be passed through
    // as AVMUTE status must be controlled by repeater (not the upstream source only)
	SiiDrvRxAudioMixGcpPassThroughConfig(0);    //配置Tx音频（非音频通道）
}


//-------------------------------------------------------------------------------------------------
//! @brief      TX Edid component initialization.
//!
//! @param[in]  isMatrixMode - indicate if device is in matrix mode (two independent pipes)
//! @retval     true - if initialization was successful.
//-------------------------------------------------------------------------------------------------
void SkAppDeviceInitEdidTx (void)
{
    SiiRegModify(REG_EDID_CTRL_XTRA, BIT_TWO_EDID_MODE, 0);
}


//-------------------------------------------------------------------------------------------------
//! @brief      Trigger an automatic hot plug event sequence (ON-OFF) on the port
//!             attached to the specified pipe.
//!
//! @param[in]  pipe    - 0: HPE on port attached to main pipe
//!                       1: HPE on port attached to roving pipe
//! @param[in]  noHpd   - true: do not include physical HPD, false - include physical HPD
//-------------------------------------------------------------------------------------------------
void SiiDrvRxPipeHpeTrigger ( unsigned int pipe, unsigned char noHpd )
{
    unsigned char triggerMask;

    triggerMask = pipe ? (BIT_OVR_RP | BIT_TRIGGER_RP) : (BIT_OVR_MP | BIT_TRIGGER_MP);
    if ( noHpd )
    {
        // Set end time of HPD HPE timer to 0 so that the auto-HPE hardware
        // will NOT controlled during HPE sequence.
        SiiRegWrite( REG_IP_HPE_HPD_END, 0x00 );
    }
    else
    {
        // Set end time of HPD HPE timer to 1200ms.
        SiiRegWrite( REG_IP_HPE_HPD_END, 0x0C );
    }

    // Toggle the HW HPE for the selected pipe, leave HPE in automatic hardware control
    SiiRegModify( REG_HPE_TRIGGER, triggerMask, SET_BITS );
    SiiRegModify( REG_HPE_TRIGGER, triggerMask, CLEAR_BITS );
}




//------------------------------------------------------------------------------
// Function:    SiiDrvDeviceSpResDetectionEnable
// Description: Enable resolution change detection in Sub Pipe.
//------------------------------------------------------------------------------

void SiiDrvDeviceSpResDetectionEnable(unsigned char isEnabled)
{
    SiiRegBitsSet(REG_INT_MASK_20, BIT_SP_HRES_CHG | BIT_SP_VRES_CHG, isEnabled);
}


//-------------------------------------------------------------------------------------------------
//! @brief      Enable or disable the the HDCP DDC bus for a Rx port.
//!
//! @param[in]  portIndex - SiiPORT_x           - Rx port (0-3).
//!                         SiiPORT_ALL         - All ports are acted on simultaneously.
//! @param[in]  setEnable - true - enable, false - disable
//-------------------------------------------------------------------------------------------------
void SiiDrvRxHdcpDdcControl (unsigned char portIndex, unsigned char setEnable )
{
    unsigned char enableVal, enableMask;

    enableVal = setEnable ? SET_BITS : CLEAR_BITS;

    if ( portIndex == SiiPORT_ALL )     // All ports at once
    {
        enableMask = VAL_DDC_PORT_ALL_EN;
    }
    else                                // Port 0 - 3
    {
        enableMask = (BIT_DDC0_EN << portIndex);
    }

    SiiRegModify( REG_RX_HDCP_DDC_EN, enableMask, enableVal );	//0xB0:0x09  RX_HDCP_DDC_EN	   1 : enable
	printf("   -- REG_RX_HDCP_DDC_EN  0xB0:0x09  MASK: %02X  Val: %02X  \r\n",(unsigned int)enableMask, (unsigned int)enableVal);
}


//-------------------------------------------------------------------------------------------------
//! @brief      Set the physical state of the RX termination for a Rx port.
//!             If the port is configured as MHL the termination is set to tri-state on enable
//!
//! @param[in]  portIndex - SiiPORT_x           - Rx port (0-3).
//! @param[in]  setEnable - true - enable, false - disable
//!
//! @warning    The 'portIndex' parameter value 0xFF should not be used unless
//! @warning    all ports are HDMI1.3/a (not MHL or CDC)
//-------------------------------------------------------------------------------------------------
void SiiDrvRxTermControl ( int portIndex, unsigned char setEnable )
{
    unsigned char enableVal, rPwrStatus;
    unsigned char enableMask = 0;


	// if the port is not connected we don't want to terminate that one.
	// However, if setEnable is false we don't care and just unterminate the port, there
	// are cases when you have to unterminate the port in Matrix Switch mode even when the
	// port is connected to source
	SiiRegWrite( REG_TMDS0_CTRL2, 0x00);
	//   DEBUG_PRINT( MSG_ERR, "\n******SiiPortType_HDMI******");
	// PLL configuration
	SiiRegWrite( REG_TMDS0_CTRL3, 0x00);

#if 0	
	if(setEnable)
	{
		// see if the port is connected to a source or not
		rPwrStatus = SiiRegRead( REG_PWR5V_STATUS ) & MSK_PWR5V_ALL;
		if(!((rPwrStatus >> portIndex) & BIT0))
		{
			setEnable = 0;
		}
	}
#endif
		
	/*
	DEBUG_PRINT( MSG_DBG, "SiiDrvRxTermControl , portIndex:: %02X, setEnable:: %02X\n\n\n",
	portIndex, setEnable );
	*/
	
	enableVal = setEnable ? VAL_TERM_ALL_ON : VAL_TERM_ALL_OFF;
	
	// Move enable value bits to correct position
	enableMask  = (MSK_TERM << ((portIndex % 4) * 2));
	SiiRegModify( REG_RX_TMDS_TERM_0, enableMask, enableVal );	// 0xB0:0x82 RX_TMDS_TERM_0

	//SiiRegModify( REG_RX_TMDS_TERM_0, enableMask, 0x00 );	// 0xB0:0x82 RX_TMDS_TERM_0

	printf("   -- RX_TMDS_TERM_0      0xB0:0x82  MASK: %02X  Val: %02X  \r\n",(unsigned int)enableMask, (unsigned int)enableVal);
	// DEBUG_PRINT(MSG_ALWAYS,"\n HDMI TERMINATION ***********************************88888\n");
	// DEBUG_PRINT(MSG_ALWAYS,"\n HDMI TERMINATION ***************************Mask: %02X val: %02X \n",enableMask,  enableVal);	
}

//-------------------------------------------------------------------------------------------------
//! @brief      Enable or disable the the EDID DDC bus for a Rx port.
//!
//! @param[in]  portIndex - SiiPORT_x           - Rx port (0-3).
//!                         SiiPORT_ALL         - All ports are acted on simultaneously.
//! @param[in]  setEnable - true - enable, false - disable
//-------------------------------------------------------------------------------------------------
void SiiDrvRxEdidDdcControl ( unsigned char portIndex, unsigned char setEnable )
{
    unsigned char enableVal, enableMask;

    enableVal = setEnable ? SET_BITS : CLEAR_BITS;

    if ( portIndex == SiiPORT_ALL )     // All ports at once
    {
        enableMask = VAL_EN_DDC_ALL;
    }
    else                                // Port 0 - 3
    {
        enableMask = (BIT_EDDC_EN0 << portIndex);
    }

    SiiRegModify( RX_EDID_DDC_EN, enableMask, enableVal );
    printf("   -- RX_EDID_DDC_EN      0xE0:0x01  MASK: %02X  Val: %02X  \r\n",(unsigned int)enableMask, (unsigned int)enableVal);
}

//-------------------------------------------------------------------------------------------------
//! @brief      Set the physical state of the Hot Plug Detect (HPD) signal for a Rx port.
//!             If the port is configured as MHL the HPD line is set to tri-state on enable
//!
//! @param[in]  portIndex - SiiPORT_x           - Rx port (0-3).
//!                         SiiPORT_ALL         - All ports are acted on simultaneously.
//! @param[in]  setActive - true - set HPD to active (high or tri-state), false - set HPD to low state
//-------------------------------------------------------------------------------------------------
void SiiDrvRxHpdControl ( unsigned char portIndex, unsigned char setActive )
{
    unsigned char  enableVal, enableMask;

    enableVal = setActive ?  VAL_HP_PORT_ALL_HI : VAL_HP_PORT_ALL_LO;

    if ( portIndex == SiiPORT_ALL ) // All at once, must be HDMI ports.
    {
        SiiRegWrite( REG_HP_CTRL1, enableVal );	   
    }
    else
    {
#if 0
        // If MHL port, enable means tri-state
        if ( pDrvSwitch->portType[ portIndex] == SiiPortType_MHL )
        {
            enableVal = setActive ? VAL_HP_PORT_ALL_MHL : enableVal;
        }
#endif	
	
        // Find correct control register and update.
        SiiRegModify( REG_HP_CTRL1, (VAL_HP_PORT0_MASK << (portIndex * 2)), enableVal );

		printf("   -- RX_HP_CTRL1         0xB0:0x10  MASK: %02X  Val: %02X  \r\n",(unsigned int)enableMask, (unsigned int)enableVal);
    }
}



//------------------------------------------------------------------------------
// Function:    SiiDrvDeviceStart
// Description: Enables the device for normal operation.  This function performs
//              the procedures necessary for correct operation of the device after
//              all other initialization has been performed.
// Parameters:  none
// Returns:     true if the release is successful and the device is ready for
//              operation or false if some failure occurred.
//------------------------------------------------------------------------------

void SiiDrvDeviceStart ( void )
{
    unsigned char i=0;

	printf("9、SiI9533 Rx Config \r\n");
	printf("   - Select TMDS Termination mode(RX_TMDS_TERM_0 0xB0:0x82) \r\n");
	printf("   - Rx EDID and HDCP DDC access(RX_EDID_DDC_EN 0xE0:0x01) \r\n");
	printf("   - Rx HPD control(RX_HP_CTRL1 0xB0:0x10 ) \r\n");
	printf("   - Rx HDCP DDC enable(RX_HDCP_DDC_EN 0xB0:0x09) \r\n");

	for ( i = 0; i < SII_INPUT_PORT_COUNT; i++ )
	{
		// Set the state for this port
	    //SiiDrvRxHdcpDdcControl (i, 1);
		SiiDrvRxTermControl (i, 1 );	
		SiiDrvRxEdidDdcControl ( i, 1 );
		SiiDrvRxHpdControl ( i, 1 );     //important
		SiiDrvRxHdcpDdcControl (i, 1);
	}	 

    //SiiDrvDeviceSpResDetectionEnable(0) ;
}



//------------------------------------------------------------------------------
// Function:    SkAppDeviceInitRepeater
// Description: Perform any board-level initialization required at the same
//              time as Repeater component initialization
// Parameters:  isPowerUp
//              - true:  called when power up
//				- false: called to change the mode on fly

// Returns:
//				- true:  success
//				- false: fail
//------------------------------------------------------------------------------
void SkAppDeviceInitRepeater( void )
{
    SiiRegWrite(REG_HDCP_KSV_FIFO_CTRL, VAL_HDCP_FIFO_VALID);
	//SiiRegWrite(REG_HDCP_BCAPS_SET,	VAL_P3_REPEATER | VAL_P2_REPEATER | VAL_P1_REPEATER | VAL_P0_REPEATER);
	//Disable repeater bit for port 0-3
	SiiRegWrite(REG_HDCP_BCAPS_SET, CLEAR_BITS);
	SiiRegModify(REG_HDCP_KSV_FIFO_CTRL, VAL_HDCP_FIFO_VALID, SET_BITS);
}

//------------------------------------------------------------------------------
// Function:    SiiDrvGpioPinType
// ! @brief     Enable/disable the passed pin alternative function
// Parameters:  pins -- GPIO pins to be configured, GPIO0 == 0x0001,
//                      GPIO1 == 0x0002, etc.
//              type -- one of SiiGpioTypes_t
// Returns:     true if success, false if requested type not available for
//              one or more of the specified pin(s).
//------------------------------------------------------------------------------

unsigned char SiiDrvGpioPinType ( unsigned short pins, SiiGpioPinTypes_t pinType )
{
    unsigned char  success = 1;
    unsigned char  pinsPwdHi, pinsPwdLo, pinsAon, pinsPwdAud;

    pinsPwdHi   = (unsigned char)((pins >> 10) & MSK_ALTEN_12_10);
    pinsPwdLo   = (unsigned char)(pins >> 2);
    pinsAon     = (unsigned char)(pins) & MSK_ALTEN_0;
    pinsPwdAud  = (unsigned char)(((pins >> 1) & MSK_ALTEN_0) << 6);

    // Set the requested pin(s) to GPIO mode.
    if ( pinType == SII_GPIO_STANDARD )
    {
        // Clear all specified pins to GPIO functionality
        SiiRegBitsSet( REG_GPIO_AON_CTRL0, pinsAon, 0 );
        SiiRegBitsSet( REG_GPIO_ALTEN, pinsPwdLo, 0 );
        SiiRegBitsSet( REG_GPIO_ALTEN_DIR, pinsPwdHi, 0 );
        SiiRegBitsSet( REG_GPIO_DIR, pinsPwdAud, 0 );

    }
    else
    {
        // Determine if pins requested are valid against the pin type requested
        if (( pinType & pins & l_pinTypeMask ) != pins )
        {
            success = 0;
        }
        else
        {
            // Enable the alternate pin function for the requested pin(s)
            SiiRegBitsSet( REG_GPIO_AON_CTRL0, pinsAon, 1 );
            SiiRegBitsSet( REG_GPIO_ALTEN, pinsPwdLo, 1 );
            SiiRegBitsSet( REG_GPIO_ALTEN_DIR, pinsPwdHi, 1 );
            SiiRegBitsSet( REG_GPIO_DIR, pinsPwdAud, 1 );
            // Choose primary or secondary alternate function.
            if (( pinType & SII_GPIO_ALTFUNCTION ) == SII_GPIO_ALTFUNCTION )
            {
                pinsPwdHi =  (pins & (SII_GPIO_PIN_9| SII_GPIO_PIN_10 | SII_GPIO_PIN_11 )) ? BIT_GPIO_SEC_SEL0 : 0;
                pinsPwdHi |= (pins & (SII_GPIO_PIN_1 | SII_GPIO_PIN_2 | SII_GPIO_PIN_3)) ? BIT_GPIO_SEC_SEL1 : 0;
                pinsPwdHi |= (pins & (SII_GPIO_PIN_4 | SII_GPIO_PIN_5 )) ? BIT_GPIO_SEC_SEL2 : 0;
                SiiRegBitsSet( REG_GPIO_PRI_SEC_SEL, pinsPwdHi, 1 );
            }
        }
    }

    return( success );
}

//------------------------------------------------------------------------------
// Function:    SkAppRxAudioConfigLayout
// Description:
// Parameters:  none
// Returns:     none
//------------------------------------------------------------------------------
static void RxAudioConfigLayout(unsigned char audLayout)
{
	
    if (audLayout == SII_AUD_MULTI_CHANNEL)
    {
		SiiDrvGpioPinType(SII_GPIO_PIN_1, SII_GPIO_ALT_SD1);
		SiiDrvGpioPinType(SII_GPIO_PIN_2, SII_GPIO_ALT_SD2);
		SiiDrvGpioPinType(SII_GPIO_PIN_3, SII_GPIO_ALT_SD3);
    }
    else
    {
    	SiiDrvGpioPinType(SII_GPIO_PIN_1, SII_GPIO_STANDARD);
    	SiiDrvGpioPinType(SII_GPIO_PIN_2, SII_GPIO_STANDARD);
    	SiiDrvGpioPinType(SII_GPIO_PIN_3, SII_GPIO_STANDARD);
    }

}

//------------------------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Function:    SiiDrvRxAudioReset
// Description:  Reset the audio
// Parameters:  
// Returns:     n/a
//------------------------------------------------------------------------------
void SiiDrvRxAudioReset(unsigned char audResetType)
{
	SiiRegModify( REG_AUD_RST, audResetType, SET_BITS);
	SiiRegModify( REG_AUD_RST, audResetType, CLEAR_BITS);
}

void SiiDrvRxAudioInitMclk(void)
{
	SiiRegWrite(REG_AUDIOAAC_MCLK_SEL,0xc0 | (SII_AUD_MCLK_512 << 0x00));
	SiiRegModify( REG_AUDIOFREQ_SVAL, 0xF0, 0x00);	// ACR Audio Frequency Register

	SiiDrvRxAudioReset(0x02);
}


//------------------------------------------------------------------------------
// Function:    SiiDrvRxAudioInit
// Description:  Initialize the audio for both main and sub channel
// Parameters:  
// Returns:     n/a
//------------------------------------------------------------------------------
void SiiDrvRxAudioInit ( void )
{
	SiiRegWrite( REG_APLL_POLE, 0x88);  //set pll config #1
	SiiRegWrite( REG_APLL_CLIP, 0x16);  //set pll config #2

	SiiRegWrite( REG_AUDIO_INTR11, 0xFF); //clear the audio status
	SiiRegWrite( REG_AUDIOAUD_INTR12, 0xFF);
	SiiRegWrite( REG_AUDIOAUD_INTR13, 0xFF);

    //SiiRegWriteBlock( REG_AUDIOAEC_EN1, intStatus, 3);
 	SiiRegWrite( REG_AUDIOAEC_EN1, 0xC1); //clear the audio status
	SiiRegWrite( 0x0F15, 0x97);
	SiiRegWrite( REG_AEC_EN3, 0x01);

	SiiRegModify(REG_INT_ENABLE_IP4, 0x40, SET_BITS);

    //SiiRegWriteBlock(REG_AUDIOAEC3_CTRL, intStatus, 3);
	SiiRegWrite( REG_AUDIOAEC3_CTRL, 0x03); //clear the audio status
	SiiRegWrite( REG_AUDIOAEC2_CTRL, 0x0D);
	SiiRegWrite( REG_AUDIOAEC1_CTRL, 0x17);

    SiiRegModify(REG_AUDIOAAC_MCLK_SEL, 0xC0, 0xC0);    //add for unmute timing    

    SiiRegWrite( REG_AUDIOAUD_CTRL,  0x3c );
    SiiRegModify( REG_AUDIO_I2S_CTRL2, 0x08, 0x08);  //enable MCLK    
    SiiRegModify( REG_AUDIOHDMI_MUTE, 0x02, 0x00);   
    
    SiiRegModify( REG_AUDIOAEC_CTRL, 0xA3, 0xFF);        //enable auto video/audio configuration
    // Do not set BIT_AAC_ALL
    //SiiRegModify( REG_AUDIOAEC_CTRL,   BIT_AAC_EN|BIT_AAC_ACR_EN | BIT_AAC_OE /*0xB7*/, SET_BITS);        //enable auto video/audio configuration

	SiiDrvRxAudioInitMclk();
	//SiiDrvRxAudioEnableCHSTINT();
}

//------------------------------------------------------------------------------
// Function:    SiiDrvRxSpdifOutEnable
// Description:  Enable or disable SPDIF output.
//               NOTE: In Sub Pipe, disabling SPDIF changes the pin assignment to I2S.
// Parameters:
// Returns:     n/a
//------------------------------------------------------------------------------

void SiiDrvRxSpdifOutEnable(unsigned char isEnabled)
{
    SiiRegBitsSet(REG_AUDIOAUD_CTRL, 0x01, isEnabled);
}

//------------------------------------------------------------------------------
// Function:    SiiRxAudioInit
// Description:  Initialize the audio state machine and audio hardware start up
//              for both main and sub channel
// Parameters:  none
// Returns:     none
//------------------------------------------------------------------------------
void SiiRxAudioInit( void )
{
    SiiDrvRxAudioInit();
    SiiDrvRxSpdifOutEnable(DISENABLE); // SPDIF digital outputs are disabled by default
}

//------------------------------------------------------------------------------
// Function:    SiiDrvRxAudioInterruptMaskEnable
// Description:  Enable the mask to assert the global (PIN) interrupts.
// Parameters:  
// Returns:     n/a
//------------------------------------------------------------------------------
void SiiDrvRxAudioIntMaskEnable( unsigned char isEnable )
{
    unsigned char intStatus[3] = { 0x00, 0x00, 0x00 };

    // Enable/Disable AAC hardware function
    SiiRegModify(REG_AUDIOAEC_CTRL, 0x01, (isEnable) ? SET_BITS : CLEAR_BITS);

    if (isEnable)
    {
		intStatus[0] = 0x04;
		intStatus[1] = 0xC0;
        //intStatus[1] = BIT_AAC_DONE;
        //intStatus[2] = BIT_FSCHG;
		//SiiRegWrite(REG_AUDIO_CHAN_SEL, 0x0);
		//SiiRegWrite(REG_AUDRX_CTRL_P1, 0x30);
    }
	//SiiRegWriteBlock( 0x00A5,  intStatus, 3);	 // 0xB0:0xA5
	SiiRegWrite( 0x00A5, intStatus[0]); 
	SiiRegWrite( 0x00A6, intStatus[1]);
	SiiRegWrite( 0x00A7, intStatus[2]);

	SiiRegModify(REG_INT_ENABLE_IP5, 0x04, isEnable ? SET_BITS : CLEAR_BITS);
	SiiRegModify(REG_AUDIOACR_CTRL3, 0x78, 0x04 << 0x03);  //set ACR interrupt threshold
}


//------------------------------------------------------------------------------
// Function:    SkAppDeviceInitAudio
// Description:  Init the audio extraction for main channel only
// Parameters:  none
// Returns:     none
//------------------------------------------------------------------------------
void SkAppDeviceInitAudio( void )
{
	SiiDrvGpioPinType(SII_GPIO_PIN_7, SII_GPIO_ALT_I2S_WS0_OUT);
	RxAudioConfigLayout(SII_AUD_TWO_CHANNEL);

	SiiRxAudioInit();
	// Enable SPDIF in MP and disable it in SP (in favor of I2S stereo output)
	SiiDrvRxSpdifOutEnable(ENABLE);

	//Only enable main pipe int mask
    SiiDrvRxAudioIntMaskEnable(ENABLE);

#if 0
	SiiRegModify(0x10FB, 0x10, 0x00);	 //0x72:0xFB

	// Enable Video clock counter
    SiiRegModify(0x0F62, 0x01, 0xff);	   //0xF8:0x62

	// AUDIO_CFG3 2014-12-27 15-39
	SiiRegModify(0x1026,0x80,0xff);	     //0x72:0x26

	SiiRegWrite(0x0037,0x48);
#endif
}


void setrx_edid(char index,char select)
{
    unsigned char dat=0;

	SiiRegModify(0x0904, 0x17, index - 1);
	SiiRegWrite( 0x0902, 0x00);

    dat = I2C_BufferWrite_Number( 0xE0,0x03,Edid_data_all[select],256);

	if(dat == 1)
		printf("write edid %d ok ---->  \r\n",index);
	else
		printf("write edid %d error ---->  \r\n",index);

	SiiRegWrite( 0x0905, 0x03);
}


//-------------------------------------------------------------------------------------------------
//! @brief      Enable/Disable HDCP encryption of HDMI data.
//!
//! @param[in]  isEnabled - true: to enable encryption if authentication succeeded,
//!                         false: to disable encryption (even after successful authentication).
//-------------------------------------------------------------------------------------------------
void SiiDrvTpiHdcpEncriptionEnable(unsigned char isEnable)
{
    SiiRegModify(REG_TPI__HDCP_CTRL, BIT_TPI__HDCP_CTRL__ENCRYPT_DISABLE, isEnable ? CLEAR_BITS : SET_BITS);
}


//-------------------------------------------------------------------------------------------------
//! @brief      Set dynamic or static Link Integrity mode.
//!
//!             If downstream repeater is discovered, the dynamic mode shall be enabled.
//!             It forces the chip logic to re-authenticate any time the incoming clock
//!             resolution changes. Dynamic mode works for non-repeater sink. However
//!             the authentication may take a little longer time.
//!
//! @param[in]  isEnabled - true for dynamic, false for static mode.
//-------------------------------------------------------------------------------------------------
void SiiDrvTpiHdcpDynamicAuthenticationEnable(unsigned char isEnabled)
{
    SiiRegModify(REG_TPI__SYSTEM_CONTROL, BIT_TPI__SYSTEM_CONTROL__REAUTH_EN, isEnabled ? SET_BITS : CLEAR_BITS);
}


//----------------------------------------------------------------------------------------------------------------------------------------------------------//

void sii_9533_Init(void)
{
    unsigned char test=0;
	unsigned int devTypex=0;

	I2CBUS = SiI_I2CBUS ;

    //--------------------------------------------------- SiI9533复位
	printf("1、SiI9533 Reset \r\n");
    SII_9533_RESET_H();
	sii_9533_delay(100);
    SII_9533_RESET_L();	     // 低电平复位	
	sii_9533_delay(500);
    SII_9533_RESET_H();
	sii_9533_delay(100);

	//--------------------------------------------------- SiI9533 ID and REV
	printf("2、SiI9533 Get ID and REV \r\n");
 	SiiDrvDeviceIdGet();	 //	读取9533的ID
	SiiDrvDeviceRevGet();	 //	读取版本号（通过读取ID、版本号可以验证I2C通信是否成功） 

	//--------------------------------------------------- 配置时钟
	printf("3、SiI9533 Power up boot \r\n");
    SiiDrvDevicePowerUpBoot(); //OK

	//--------------------------------------------------- 初始化寄存器	
	printf("4、SiI9533 Initialize registers \r\n");
	DrvDeviceInitRegisters();  //Initialize registers to the Programmers Reference default list.
	   
#if 1
    // Disable Auto-HPD control  wyq---20150616
    SiiRegWrite( REG_HPE_HW_CTRL_OVR, MSK_INVALIDATE_HW_HPD_CTRL ); 
#endif
	// Enable the resolution stable interrupt for use by multiple drivers
	SiiRegBitsSet( REG_INT_ENABLE_IP4, BIT_MP_RES_STABLE_CHG, 1);

	SiiRegBitsSet(REG_INT_MASK_19, BIT_MP_HRES_CHG | BIT_MP_VRES_CHG, 0);
	SiiRegBitsSet(REG_INT_MASK_20, BIT_SP_HRES_CHG | BIT_SP_VRES_CHG, 0);

     
    //setrx_edid( 3, 0) ;

	//--------------------------------------------------- 
	printf("5、SiI9533 Initialize Msw \r\n");
	SkAppDeviceInitMsw();	//OK

	printf("6、SiI9533 Initialize Tx \r\n");
	SkAppDeviceInitTx();    //OK

    //SkAppDeviceInitRepeater();      // Y/N

	printf("7、SiI9533 system reset(0xB0:0x05 0x01 0x00)   -Important- \r\n");
	printf("   Enable port change logic activity(0x50:0x3A 0x01 0x01) \r\n");
	SiiDrvDeviceRelease();            // important

	SkAppDeviceInitAudio();	          // 单独音频通道

	//SkAppDeviceInitEdidTx();		  // Y/N

	printf("8、TPI TMDS output control(TPI_INPUT_BUS_FORMAT 0x72:0x1A)    -Important- \r\n");
	SiiDrvTpiTmdsOutputEnable(1);     // important
	SiiDrvTpiHdmiOutputModeSet(1);

	SiiDrvTpiHdcpProtectionEnable(1); // Tx HDCP
	SiiDrvTpiHdcpEncriptionEnable(1);
	SiiDrvTpiHdcpDynamicAuthenticationEnable(1);

	SiiDrvTpiPowerUp();	              // important

	//SiiDrvRxPipeHpeTrigger ( 0, 1 );// Y/N

#if 0      // 4K

	SiiRegModify( 0x0303, 0x1f, 0x00 );	   // 0xFA:0x03
	SiiRegModify( 0x059F, 0x80, 0x00 );	   // 0x50:0x9F
	SiiRegModify( 0x0347, 0x80, 0x80 );	   // 0xFA:0x47
	SiiRegModify( 0x0552, 0xff, 0x03 );	   // 0x50:0x52

#endif


	SiiDrvDeviceStart();              // important
									
    sii_9533_TxEnable(ENABLE);	      // important

	printf("*********End*********  \r\n");

#if 0
	//SiiRegWrite(REG_TPI__DEVICE_POWER_STATE_CTRL, VAL_TPI__POWER_STATE_D0);	   // 0x72:0x1E 0x00

	SiiRegModify( 0x10fa, 0x7f, 0x0E );	
	SiiRegModify( 0x10f9, 0x7f, 0x00 );	

    SiiRegModify( 0x0905, 0x0f, 0x03 );	      
	sii_9533_delay(50);

    SiiRegModify( 0x0563, 0x30, 0x00 );	      // mute off

	//SiiRegModify( 0x0BFF, 0xff, 0x38 );	  // 芯片内部画面
	//SiiRegModify( 0x0BE5, 0x70, 0x50 );
	//SiiRegModify( 0x0BE0, 0x04, 0x04 );

#endif     
}

//------------------------------------------------------------------------------
// Function:    sii_9533_PortSelectSource
// Description: Connect the passed source port to the output.
//------------------------------------------------------------------------------
void sii_9533_PortSelectSource(unsigned char portIndex)
{
    /* Switch user select to the requested port.  The HPD stuff will be handled in DEM if needed.   */
    SiiRegModify( 0x000A, 0x07, portIndex );	  // 0xB0:0x0A   RX_PORT_SET   选择视频源输入端口
	printf("--- SiI9533 main pipe TMDS input port select(0xB0:0x0A 0x07 ?) \r\n");
}


void sii_9533_TxEnable(unsigned char isEnable)
{
    if(isEnable)
	{
	    SiiRegModify( 0x0050, 0x4C, 0x40 );	      // 0xB0:0x50   TX_VIDEO_SRC  使能输出端口	
	}																									    
	else
	{
	    SiiRegModify( 0x0050, 0x4C, 0x00 );	      // 0xB0:0x50   TX_VIDEO_SRC  失能输出端口	
	}

	printf("--- SiI9533 Tx0 pipe enable(0xB0:0x50 0x40 0x40) \r\n");
}

//----------------------------------------------------------------------------------------------------------------------------------------------------------//








