#include "mcu.h"

double        timer0_time=0;
unsigned int  timer0_num=0;
unsigned int  timer0_load=0;

/*
 * @brief  GPIOx Configuration.
 * @param  GPIOx : where x can be 0,1,2,3,4 to select the GPIO port.  
 *         Pin_x : where x can be 0,1,2,3,4,5,6,7,All to select the GPIO pin.
 *         GPIO_Mode : GPIO_Mode_IO��GPIO_Mode_Out_PP��GPIO_Mode_IN or GPIO_Mode_Out_OD to select the GPIO mode. 
 * @retval None
 */
void GPIO_Config(unsigned char GPIOx, unsigned char Pin_x, unsigned char GPIO_Mode)
{
    switch(GPIOx)
	{ 
		case GPIO0 : 
		    {
			    switch(GPIO_Mode)
				{
				    case GPIO_Mode_IO :
					{
						P0M1 &= ~(Pin_x);			  //0
						P0M0 &= ~(Pin_x);			  //0
					}
						break;
					case GPIO_Mode_Out_PP :
					{
						P0M1 &= ~(Pin_x);	          //0
						P0M0 |= (Pin_x);			  //1
					}
						break;
					case GPIO_Mode_IN :
					{
						P0M1 |= (Pin_x);	          //1
						P0M0 &= ~(Pin_x);	          //0
					}
						break;
					case GPIO_Mode_Out_OD :
					{
						P0M1 |= (Pin_x);	          //1
						P0M0 |= (Pin_x);              //1
					}
						break;
				}
			}
			break;
		case GPIO1 : 
			{
			    switch(GPIO_Mode)
				{
				    case GPIO_Mode_IO :
					{
						P1M1 &= ~(Pin_x);			  //0
						P1M0 &= ~(Pin_x);			  //0
					}
						break;
					case GPIO_Mode_Out_PP :
					{
						P1M1 &= ~(Pin_x);	          //0
						P1M0 |= (Pin_x);			  //1
					}
						break;
					case GPIO_Mode_IN :
					{
						P1M1 |= (Pin_x);	          //1
						P1M0 &= ~(Pin_x);	          //0
					}
						break;
					case GPIO_Mode_Out_OD :
					{
						P1M1 |= (Pin_x);	          //1
						P1M0 |= (Pin_x);              //1
					}
						break;
				}
			}
			break;
		case GPIO2 : 
		    {
			    switch(GPIO_Mode)
				{
				    case GPIO_Mode_IO :
					{
						P2M1 &= ~(Pin_x);			  //0
						P2M0 &= ~(Pin_x);			  //0
					}
						break;
					case GPIO_Mode_Out_PP :
					{
						P2M1 &= ~(Pin_x);	          //0
						P2M0 |= (Pin_x);			  //1
					}
						break;
					case GPIO_Mode_IN :
					{
						P2M1 |= (Pin_x);	          //1
						P2M0 &= ~(Pin_x);	          //0
					}
						break;
					case GPIO_Mode_Out_OD :
					{
						P2M1 |= (Pin_x);	          //1
						P2M0 |= (Pin_x);              //1
					}
						break;
				}
			}
			break;
		case GPIO3 : 
			{
			    switch(GPIO_Mode)
				{
				    case GPIO_Mode_IO :
					{
						P3M1 &= ~(Pin_x);			  //0
						P3M0 &= ~(Pin_x);			  //0
					}
						break;
					case GPIO_Mode_Out_PP :
					{
						P3M1 &= ~(Pin_x);	          //0
						P3M0 |= (Pin_x);			  //1
					}
						break;
					case GPIO_Mode_IN :
					{
						P3M1 |= (Pin_x);	          //1
						P3M0 &= ~(Pin_x);	          //0
					}
						break;
					case GPIO_Mode_Out_OD :
					{
						P3M1 |= (Pin_x);	          //1
						P3M0 |= (Pin_x);              //1
					}
						break;
				}
			}
			break;
		case GPIO4 : 
			{
			    switch(GPIO_Mode)
				{
				    case GPIO_Mode_IO :
					{
						P4M1 &= ~(Pin_x);			  //0
						P4M0 &= ~(Pin_x);			  //0
					}
						break;
					case GPIO_Mode_Out_PP :
					{
						P4M1 &= ~(Pin_x);	          //0
						P4M0 |= (Pin_x);			  //1
					}
						break;
					case GPIO_Mode_IN :
					{
						P4M1 |= (Pin_x);	          //1
						P4M0 &= ~(Pin_x);	          //0
					}
						break;
					case GPIO_Mode_Out_OD :
					{
						P4M1 |= (Pin_x);	          //1
						P4M0 |= (Pin_x);              //1
					}
						break;
				}
			}
			break;
	}
}


/*
 * @brief  UARTx Initialize.
 * @param  UARTx: where x can be 0, 1 to select the UART peripheral.(UART0:P3, UART1:P1)  
 *         baud : baud set.
 * @retval None
 */
void UART_Init(unsigned char UARTx, unsigned long baud)
{
	AUXR1 = UARTx;	 	
	AUXR |= 0x40;    	 
	
	SCON |= 0x50;	 
	PCON  = 0x00;     
	
	TMOD &= 0x0F;	
	TMOD |= 0x20;	 
	TH1 = TL1 = -(OSC/32/baud) ;   
	TR1 = 1;		 
		
	ES = 1;		  
	PS = 0;       
	EA = 1;	
	TI = 0;	  
}

char putchar(char c)
{
    SBUF = c;
    while(!TI);
	TI = 0;
	//ES = 1;

	return c;
}


/*
 * @brief  Initializes the EXTI0  peripheral according to the specified	parameters .        
 * @param  EXTIx: where x can be 0, 1 to select the EXTI peripheral.
 *         EXTI_Mode: Trigger_Falling or Trigger_Low to select the EXTI0 Trigger mode. 
 *         EXTI_Priority: Priority_High or Priority_Low to select the EXTI0 Priority.
 * @retval None
 */
void EXTI_Init(unsigned char EXTIx, unsigned char EXTI_Mode, unsigned char EXTI_Priority)
{
    if(EXTIx==EXTI0)
	{
	    IT0 = EXTI_Mode;
		PX0 = EXTI_Priority;
		EX0 = 1;
		EA = 1;
	}
	else if(EXTIx==EXTI1)
	{
		IT1 = EXTI_Mode;
		PX1 = EXTI_Priority;
		EX1 = 1;
		EA = 1;
	}
}


/*
 * @brief  Deinitializes the Timer0 peripheral .
 * @param  us: Set timing time, microsecond(us), time range: 10<us<5000
 * @retval None
 */
void TIMER0_Init(unsigned int us)   //  10<us<5000
{
    timer0_time = us ;

    AUXR |= 0x80;     
	
	TMOD &= 0xF0;    
	TMOD |= 0x01;     
	timer0_load = 65536 - timer0_time/1000000*OSC ;
	TH0 = (unsigned char)(timer0_load>>8);
	TL0 = (unsigned char)(timer0_load);
	ET0 = 1;
	EA = 1;           
	TR0 = 1;          
}


/*
 * @brief  Delay .
 * @param  ms: Set delay time, millisecond(ms).
 * @retval None
 */
void delay_ms(unsigned int ms)
{
    timer0_num = ms/(timer0_time/1000);

	while(timer0_num);   
}

