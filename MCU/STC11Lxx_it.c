#include <stc11lxx.h>
#include "mcu.h"
#include <string.h>

extern unsigned int  timer0_num;
extern unsigned int  timer0_load;

/********************************中断处理函数***************************************/
void EXTI0_ISR() interrupt 0
{
//----------------------请在分割线内添加外部中断0中断处理函数---------------------//

		

//--------------------------------------------------------------------------------//
}


void Timer0_ISR() interrupt 1
{
	timer0_num--;

//------------------------请在分割线内添加定时器0中断处理函数---------------------//



//--------------------------------------------------------------------------------//

	TH0 = (unsigned char)(timer0_load>>8);
	TL0 = (unsigned char)(timer0_load);
}


void EXTI1_ISR() interrupt 2
{
//----------------------请在分割线内添加外部中断1中断处理函数---------------------//



//--------------------------------------------------------------------------------//
}


void Timer1_ISR() interrupt 3
{
//------------------------请在分割线内添加定时器1中断处理函数---------------------//



//--------------------------------------------------------------------------------//
}

unsigned char uart_test[5]={0};
unsigned char flag=0;
unsigned char UART_FLAG = 0;
unsigned char i=0;

// W   page addr mark data
// R   page addr mark 
void UART_ISR()   interrupt 4 
{
    unsigned char dat=0;
	
	if(RI)
	{
	    RI = 0; 
//----------------------请在分割线内添加串口接收中断处理函数---------------------//
 		dat = SBUF;
		if((flag == 1) && (UART_FLAG == 0))		     // write register	  4
		{
		    uart_test[i] =  dat;
			i++;
			if(i==4)	//  Receive end
			{
				i = 0;
				UART_FLAG = flag;
				//flag=0;
			}
		}
		else if((flag == 2) && (UART_FLAG == 0))     // read register    3
		{
		    uart_test[i] =  dat;
			i++;
			if(i==3)    //  Receive end
			{
				i = 0;
				UART_FLAG = flag;
				//flag=0;
			}
		}
#if 0
		else if((flag == 3) && (UART_FLAG == 0)) 
		{
		   	//i = 0;
		    UART_FLAG = flag;
			//flag=0;
		}
		else if((flag == 4) && (UART_FLAG == 0)) 
		{
		   	//i = 0;
		    UART_FLAG = flag;
			//flag=0;
		}
#endif

		 
		if( (dat == 'W') && (UART_FLAG ==0) )        // write
		{
		    i = 0;
			flag = 1;  
			memset(uart_test,0,5); 
		}
		else if( (dat == 'R') && (UART_FLAG == 0) )  // read
		{
			i = 0;
			flag = 2;
			memset(uart_test,0,5); 
		}
#if 0
		else if( (dat == 'A') && (UART_FLAG == 0) )  // read all
		{
		    //i = 0;
			flag = 3;
			//memset(uart_test,0,5); 
		}
		else if( (dat == 'I') && (UART_FLAG == 0) )  // init
		{
			//i = 0;
			flag = 4;
			//memset(uart_test,0,5); 
		}

#endif

//--------------------------------------------------------------------------------// 
	}
	if(TI)
	{
	    TI = 0;   
	}
}
