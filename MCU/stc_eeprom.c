#include "stc_eeprom.h"

/*
 * @brief  Disable EEPROM function, make MCU in a safe state .
 * @param  None
 * @retval None
 */
void STC_EEPROM_Idle(void)
{
    IAP_CONTR = 0;
	IAP_CMD = 0;
	IAP_TRIG = 0;
	IAP_ADDRH = 0x80;
	IAP_ADDRL = 0x00;
}

/*
 * @brief  Write one byte data to EEPROM  .
 * @param  addr: 
 *         dat :
 * @retval None
 */
void STC_EEPROM_WriteByte(unsigned int addr, unsigned char bytedata)
{
    IAP_CONTR = IAP_ENABLE;
	IAP_CMD = IAP_CMD_WRITE;
	IAP_ADDRH = addr>>8;
	IAP_ADDRL = addr;
	IAP_DATA = bytedata;
	IAP_TRIG = 0x5A;
	IAP_TRIG = 0xA5; 
	_nop_();

	STC_EEPROM_Idle();
}

/*
 * @brief  Resd one byte data from EEPROM .
 * @param  addr: The address of the data .
 * @retval data.
 */
unsigned char STC_EEPROM_ReadByte(unsigned int addr)
{
    unsigned char dat=0;

	IAP_CONTR = IAP_ENABLE;
	IAP_CMD = IAP_CMD_READ;
	IAP_ADDRH =	addr>>8;
	IAP_ADDRL =	addr;
	IAP_TRIG = 0x5A;
	IAP_TRIG = 0xA5;
	_nop_();

	dat = IAP_DATA;
    STC_EEPROM_Idle();

    return dat;
}

/*
 * @brief  EEPROM erase one sector(1 sector:512 Byte) .
 * @param  addr: 0x0000 or 0x0200.(STC11L60XE EEPROM is 1K(sector1:0x0000~0x01FF   sector2:0x0200~0x03FF))
 * @retval None
 */
void STC_EEPROM_EraseSector(unsigned int addr)
{
	IAP_CONTR = IAP_ENABLE;
	IAP_CMD = IAP_CMD_ERASE;
	IAP_ADDRH =	addr>>8;
	IAP_ADDRL =	addr;
	IAP_TRIG = 0x5A;
	IAP_TRIG = 0xA5; 
	_nop_();

	STC_EEPROM_Idle();   
}





