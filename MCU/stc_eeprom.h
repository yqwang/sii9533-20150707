#ifndef __STC_EEPROM_H
#define __STC_EEPROM_H	
#include <stc11lxx.h>
//#include "includes_bsp.h"
#include <intrins.h>

#define IAP_CMD_STANDBY  0x00
#define IAP_CMD_READ     0x01
#define IAP_CMD_WRITE    0x02
#define IAP_CMD_ERASE    0x03

//#define IAP_ENABLE       0x80   //if OSC<30MHz
//#define IAP_ENABLE       0x81   //if OSC<24MHz
//#define IAP_ENABLE       0x82   //if OSC<20MHz
#define IAP_ENABLE       0x83   //if OSC<12MHz
//#define IAP_ENABLE       0x84   //if OSC< 6MHz
//#define IAP_ENABLE       0x85   //if OSC< 3MHz
//#define IAP_ENABLE       0x86   //if OSC< 2MHz
//#define IAP_ENABLE       0x87   //if OSC< 1MHz

void STC_EEPROM_Idle(void);
void STC_EEPROM_WriteByte(unsigned int addr, unsigned char bytedata);
unsigned char STC_EEPROM_ReadByte(unsigned int addr);
void STC_EEPROM_EraseSector(unsigned int addr);
		 				    
#endif

/**********************************************************************************************************************
*                                      STC11L60XE内部自带EEPROM使用说明
*1、STC11L60XE内部EEPROM为1K，分为2个扇区（sector1:0x0000~0x01FF   sector2:0x0200~0x03FF）。
*2、函数使用:
*		     void STC_EEPROM_Idle(void);  //设置STC内部EEPROM为空闲状态
*			 void STC_EEPROM_WriteByte(unsigned int addr, unsigned char bytedata); //向EEPROM的某个地址写一个字节的数据
*			 unsigned char STC_EEPROM_ReadByte(unsigned int addr);	//从EEPROM的某个地址读取一个字节的数据
*			 void STC_EEPROM_EraseSector(unsigned int addr);  //擦除一个扇区（注意这里只能一个扇区一个扇区的擦除）
***********************************************************************************************************************/


