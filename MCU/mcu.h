#ifndef __MCU_H
#define __MCU_H	 
#include <stc11lxx.h>
//#include "includes_bsp.h"


/*******************************SYSTEM********************************/
#define OSC              11059200


/********************************GPIO*********************************/
#define GPIO0            0		 //P0
#define GPIO1            1		 //P1
#define GPIO2            2		 //P2
#define GPIO3            3		 //P3
#define GPIO4            4		 //P4

#define Pin_0            (0x01)  //< Pin 0  selected 
#define Pin_1            (0x02)  //< Pin 1  selected 
#define Pin_2            (0x04)  //< Pin 2  selected 
#define Pin_3            (0x08)  //< Pin 3  selected 
#define Pin_4            (0x10)  //< Pin 4  selected 
#define Pin_5            (0x20)  //< Pin 5  selected 
#define Pin_6            (0x40)  //< Pin 6  selected 
#define Pin_7            (0x80)  //< Pin 7  selected 
#define Pin_All          (0xFF)  //< Pin All  selected 

#define GPIO_Mode_IO	 0x00	 //准双向口（传统8051的I/O模式）
#define GPIO_Mode_Out_PP 0x01	 //推挽输出
#define GPIO_Mode_IN	 0x02    //仅为输入
#define GPIO_Mode_Out_OD 0x03    //开漏输出


/********************************UART*********************************/
#define UART0            0x00    //AUXR1 = 0x00; 选用P3口的串口
#define UART1            0x80	 //AUXR1 = 0x80; 选用P1口的串口


/********************************EXTI*********************************/
#define EXTI0            0		 //外部中断0
#define EXTI1            1		 //外部中断1

#define Trigger_Falling  1       //外部中断下降沿触发
#define Trigger_Low      0       //外部中断低电平触发
#define Priority_High    1       //外部中断优先级高
#define Priority_Low 	 0       //外部中断优先级低


/******************************函数声明*******************************/
void GPIO_Config(unsigned char GPIOx, unsigned char Pin_x, unsigned char GPIO_Mode);
void UART_Init(unsigned char UARTx, unsigned long baud);
void EXTI_Init(unsigned char EXTIx, unsigned char EXTI_Mode, unsigned char EXTI_Priority);
void TIMER0_Init(unsigned int us);
void delay_ms(unsigned int ms);
		 				    
#endif

