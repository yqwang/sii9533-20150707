#include <stc11lxx.h>
#include "includes_bsp.h"


extern unsigned char uart_test[5];	  // 串口接收命令
extern unsigned char UART_FLAG;	      // 命令接收完成标志


int main(void)
{
	unsigned char rev=0;
	unsigned char test=0;
	unsigned char sii_reg=0;

    //--------------------------------------------------Init-------------------------------------------------//
	LED_Init();
	UART_Init(UART0, 115200);
	TIMER0_Init(1000);	         //参数为定时时间（单位：微秒）TIMER0_Init(unsigned int us) 10<us<5000

#if 1
	rev = 1;     //软件版本号
	printf("--------------------------------------\r\n");
	printf("K029 SiI9533 Project! \r\n");
	printf("--------------------------------------\r\n");
	printf("Rev = %d \r\n",(unsigned int)rev);
#endif

	//------------------------------------------STC11L60XE EEPROM Test---------------------------------------//
#if 1
	test=0;
	//STC_EEPROM_EraseSector(0x0000);	    //擦除只能一个一个扇区的擦除，不能一个字节擦除（1个扇区，512个字节）, STC11L60XE有2个扇区
	//STC_EEPROM_WriteByte(0x0001, 0x11);
	test = STC_EEPROM_ReadByte(0x0001);
	if(test == 0x11)
	{
	    printf("STC11L60XE EEPROM is OK! \r\n");   
	}
	else
	{
	    printf("STC11L60XE EEPROM is error! \r\n"); 
	}
#endif 

	//---------------------------------------------AT24C02 Test---------------------------------------------//
#if 1
    test = 0;
	//EEPROM_WriteByte(0x00, 0x28);
	delay_ms(10);
	test = EEPROM_ReadByte(0x00);
	if(test == 0x28)
	{
	    printf("AT24C02 is OK! \r\n");   
	}
	else
	{
	    printf("AT24C02 is error! \r\n"); 
	}
#endif

	//---------------------------------------------SiI9533 Test---------------------------------------------//
#if 1
	printf("*********SiI9533 Init********* \r\n"); 
    sii_9533_Init();
	sii_9533_PortSelectSource(SI_PORT_2);	
#endif

    //------------------------------------------------while-------------------------------------------------//
	while(1)
	{
	    //-----------------------------------------SiI9533 Debug--------------------------------------------//
		//W:写寄存器    R:读寄存器   A:读所有寄存器   I:初始化芯片
        if( UART_FLAG != 0)
		{
			if(UART_FLAG == 1)	      //write  设备地址，寄存器地址，要写的位，数据
			{
			    SiiRegModify( ((unsigned int)uart_test[0]<<8) | uart_test[1], uart_test[2], uart_test[3] );

				//SiiRegModify( 0x000A, 0x07, 0x02 );	      // 0xB0:0x0A   RX_PORT_SET   选择视频源输入端口				
				//SiiRegModify( 0x0050, 0x4C, 0x40 );	      // 0xB0:0x50   TX_VIDEO_SRC  使能输出端口					
				//SiiDrvDeviceStart();

				printf("Register write: %2X:%2X %2X <- %2X  \r\n", (unsigned int)uart_test[0],(unsigned int)uart_test[1],(unsigned int)uart_test[2],(unsigned int)uart_test[3] );
			}
			else if(UART_FLAG == 2)   //read   设备地址，寄存器地址，要写的位
			{
			    sii_reg = SiiRegRead( ((unsigned int)uart_test[0]<<8) | uart_test[1]);
				sii_reg = sii_reg & uart_test[2];
			    printf("Register read : %2X:%2X %2X -> %2X  \r\n", (unsigned int)uart_test[0],(unsigned int)uart_test[1],(unsigned int)uart_test[2],(unsigned int)sii_reg );
			}
#if 0
			//else if(UART_FLAG == 3)
			//{
				 
			//}
			else if(UART_FLAG == 4) 
			{
				SiiDrvDeviceStart();
				SiiRegModify( 0x000A, 0x07, 0x02 );	      // 0xB0:0x0A   RX_PORT_SET   选择视频源输入端口				
				SiiRegModify( 0x0050, 0x4C, 0x40 );	      // 0xB0:0x50   TX_VIDEO_SRC  使能输出端口	
				//sii_9533_Init();
				printf("SiI9533 init \r\n");
			}
#endif

			UART_FLAG = 0;
		}

		//-------------------------------------------------------------------------------------------------//
	}

	return 0;
}



